const PttBiodata = require("../models/ptt/ptt-biodata.model");
const skpdDetail = require("./detail-skpd");

const { parsePhoneNumberFromString } = require("libphonenumber-js");

module.exports = async (id) => {
  const employee = await PttBiodata.query()
    .findById(id)
    .withGraphFetched("[jabatan(aktif).[jabatan(simple)]]");

  let currentJabatan;
  const [jabatan] = employee.jabatan;

  if (!jabatan) {
    currentJabatan = null;
  }

  currentJabatan = {
    nama_jabatan: jabatan?.jabatan?.name,
    tmt_jabatan: jabatan?.tgl_mulai,
  };

  const {
    aktif: active,
    email,
    niptt,
    thn_lahir,
    nama: name,
    no_hp,
    foto,
    jk,
    id_skpd,
  } = employee;

  const fullSkpd = await skpdDetail(id_skpd);

  const result = {
    email,
    active: active === "Y" ? true : false,
    phone_number: parsePhoneNumberFromString(no_hp, "ID").number || "000000",
    niptt,
    id_skpd,
    department_fullname: fullSkpd,
    jabatan: currentJabatan?.nama_jabatan,
    tmt_jabatan: currentJabatan?.tmt_jabatan,
    birthdate: thn_lahir,
    family_name: name,
    gender: jk === "L" ? "Male" : "Female",
    given_name: name,
    locale: "ID",
    middle_name: name,
    name,
    nickname: name,
    picture: `http://bkd.jatimprov.go.id/pttpk/upload_foto/${foto}`,
    preferred_username: name,
    profile: name,
    updated_at: new Date(),
    website: `http://bkd.jatimprov.go.id/pttpk/${name}`,
    zoneinfo: "ID",
  };

  return result;
};
