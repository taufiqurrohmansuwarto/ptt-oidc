const documentModel = require("../../models/siasn_esign/documents.model");
const recipientModel = require("../../models/siasn_esign/recipients.model");

module.exports.approveSign = (req, res, data) => {
  const { accountId, otp_code, client: minioClient, trx } = data;
  return res
    .status(200)
    .json({ code: 200, message: "successfully sign document" });
};

module.exports.rejectSign = (req, res, data) => {
  return res.status(200).json({});
};
module.exports.approveReviewer = (req, res, data) => {
  return res.status(200).json({});
};
module.exports.rejectReviewer = (req, res, data) => {
  return res.status(200).json({});
};
