// create recipients based on workflow
module.exports = ({
  workflow,
  pegawaiId,
  documentId,
  upload_date,
  document_title,
}) => {
  let result;
  if (workflow === "selfSign") {
    result = {
      staff_id: pegawaiId,
      document_id: documentId,
      role: "signer",
      signatory_status: "in progress",
      is_owner: true,
      status: "draft",
      uploader: pegawaiId,
      upload_date,
      document_title,
    };
  } else if (workflow === "signAndRequest") {
    result = {
      staff_id: pegawaiId,
      document_id: documentId,
      role: "signer",
      signatory_status: "in_progress",
      sequence: 0,
    };
  } else if (workflow === "requestFromOthers") {
    result = {
      staff_id: pegawaiId,
      document_id: documentId,
      role: null,
      is_owner: true,
      status: "draft",
      uploader: pegawaiId,
      upload_date,
      document_title,
    };
  }

  return result;
};
