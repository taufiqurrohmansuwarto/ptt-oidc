const { createCanvas } = require("canvas");
const QrCode = require("qrcode");
const userModel = require("../../models/siasn_master/biodata.model");

// ini adalah penanda barcode
module.exports = async (workflow, filename, user) => {
  const width = 500;
  const height = 500;

  const canvas = createCanvas(width, height);
  QrCode.toCanvas(
    canvas,
    `${filename}_${workflow}_${user.nama}_${user.nip_baru}`
  );
  return canvas;
};
