// const data = {
//   id: "jM7939DhSygcvEwzNg2-p",
//   staff_id: 56543,
//   document_id: "iciN3WRoffXy7otZt6IYd",
//   status: "draft",
//   document: {
//     title: "Rekap_JF_Mei_2019_update (3).pdf",
//     upload_date: "2020-09-18T04:24:13.000Z",
//     recipients: [
//       {
//         staff_id: 56543,
//         role: "signer",
//         signatory_status: "in progress",
//         user: {
//           nama: "IPUT TAUFIQURROHMAN SUWARTO",
//         },
//       },
//     ],
//   },
// };

module.exports.serializeDocument = (accountId, data) => {
  if (data.length === 0) {
    return [];
  }

  return data.map((d) => {
    return {
      id: d.id,
      staff_id: d.staff_id,
      document_id: d.document_id,
      status: d.status,
      type: d.document.type,
      signatory_status: d.signatory_status,
      role: d.role,
      owner: d.document.owner.nama,
      ownerAvatar: `https://master.bkd.jatimprov.go.id/files_jatimprov/${d.document.owner.fileDiri.file_foto}`,
      workflow: d.document.workflow,
      title: d.document.title,
      upload_date: d.document.upload_date,
      recipients: d.document.recipients.map((doc) => ({
        id: doc.id,
        staff_id: doc.staff_id,
        signatory_status: doc.signatory_status,
        role: doc.role,
        status: doc.status,
        is_approve: doc.is_approve,
        nip: doc.user.nip_baru,
        name:
          parseInt(accountId) === parseInt(doc.staff_id)
            ? "You"
            : doc.user.nama,
        profile_picture: `https://master.bkd.jatimprov.go.id/files_jatimprov/${doc.user.fileDiri.file_foto}`,
      })),
    };
  });
};

module.exports.serializeHistory = (uploader, data) => {
  if (data.length === 0) {
    return [];
  } else {
    return data.map((d) => {
      return {
        id: d.id,
        document_id: d.document_id,
        type: d.type,
        action: d.action,
        name: d.user.nama,
        nip: d.user.nip_baru,
        picture: `https://master.bkd.jatimprov.go.id/files_jatimprov/${d.user.fileDiri.file_foto}`,
        is_owner: parseInt(uploader) === parseInt(d.staff_id),
        created_at: d.created_at,
      };
    });
  }
};

module.exports.serializeRecipients = (data) => {
  if (data.recipients.length === 0) {
    return data;
  } else {
    return {
      id: data.id,
      type: data.type,
      recipients: data.recipients.map((recipient) => ({
        id: recipient.id,
        staff_id: recipient.staff_id,
        role: recipient.role,
        signatory_status: recipient.signatory_status,
        name: recipient.user.nama,
        is_approve: recipient.is_approve,
        status: recipient.status,
        is_owner: recipient.is_owner,
        nip: recipient.user.nip_baru,
        profile_picture: `https://master.bkd.jatimprov.go.id/files_jatimprov/${recipient.user.fileDiri.file_foto}`,
      })),
    };
  }
};
