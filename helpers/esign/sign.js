// sign function with the client
const { PDFDocument, StandardFonts, rgb, degrees } = require("pdf-lib");
const { draw } = require("./sign-symbol");
const { nanoid } = require("nanoid");

module.exports.rejectDocument = async (props) => {
  const { documentName, client, currentUserName } = props;
  const download = (...args) => {
    return new Promise((resolve, reject) => {
      client.getObject(...args, function (err, stream) {
        if (err) {
          reject(err);
        } else {
          let buffers = [];
          stream.on("data", function (chunk) {
            buffers.push(chunk);
          });
          stream.on("end", function () {
            const buffer = Buffer.concat(buffers);
            resolve(buffer);
          });
          stream.on("error", function (err) {
            reject(err);
          });
        }
      });
    });
  };

  const ongoingDocumentBuffer = await download("esign", documentName);
  const pdfDoc = await PDFDocument.load(ongoingDocumentBuffer);
  const helveticaFont = await pdfDoc.embedFont(StandardFonts.Helvetica);
  const pages = pdfDoc.getPages();
  const firstPage = pages[0];
  const { height } = firstPage.getSize();
  firstPage.drawText(`REJECTED by ${currentUserName}`, {
    x: 5,
    y: height / 2 + 300,
    size: 50,
    font: helveticaFont,
    color: rgb(0.95, 0.1, 0.1),
    rotate: degrees(-45),
  });

  const pdfBytes = await pdfDoc.save();
  const documentSignName = `rejected_${nanoid()}.pdf`;

  return {
    pdfBytes,
    documentSignName,
  };
};

// the return value must be buffer and document name
module.exports.sign = async (props) => {
  const { client, accountId, documentName, signCoordinate } = props;

  const download = (...args) => {
    return new Promise((resolve, reject) => {
      client.getObject(...args, function (err, stream) {
        if (err) {
          reject(err);
        } else {
          let buffers = [];
          stream.on("data", function (chunk) {
            buffers.push(chunk);
          });
          stream.on("end", function () {
            const buffer = Buffer.concat(buffers);
            resolve(buffer);
          });
          stream.on("error", function (err) {
            reject(err);
          });
        }
      });
    });
  };

  const initialDocumentBuffer = await download("esign", documentName);

  console.log("ini adalah dokumen yang akan ditandatangani");

  const pdfDoc = await PDFDocument.load(initialDocumentBuffer);
  // todo implement bsre call rest api
  const images = await draw(accountId);
  const jpgImageBytes = images.toBuffer();
  const jpgImage = await pdfDoc.embedPng(jpgImageBytes);

  const originalImageHeight = jpgImage.scale(1).height;
  const originalImageWidth = jpgImage.scale(1).width;
  const pages = pdfDoc.getPages();

  signCoordinate.map(({ xPos: x, yPos: y, height, width, page }) => {
    const imageProperty = {
      height: height / originalImageHeight,
      width: width / originalImageWidth,
    };

    const clientCoord = { x, y };
    const serverPage = pages[page - 1];
    const serverCoord = {
      x,
      y: serverPage.getHeight() - (clientCoord.y + height).toFixed(2),
    };

    const serverData = {
      x: serverCoord.x,
      y: serverCoord.y,
      width: jpgImage.scale(imageProperty.width).width,
      height: jpgImage.scale(imageProperty.height).height,
    };

    serverPage.drawImage(jpgImage, serverData);
  });

  const pdfBytes = await pdfDoc.save();
  const documentSignName = `ongoing_${nanoid()}.pdf`;

  return {
    pdfBytes,
    documentSignName,
  };
};
