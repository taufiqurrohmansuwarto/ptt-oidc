// ini adalah esign api yang ada pada dokumentasi esign setiap kali kita masuk
const axios = require("./axios");
const FormData = require("form-data");

// API USER OPERATION
const urlUserApi = `${process.env.ESIGN_API_URL}/api/user`;

/**
 *
 * INI ADALAH WEB SERVICE UNTUK MENGGUNAKAN ESIGN JATIM
 */

// service untuk melakukan sign dokumen
module.exports.sign = (file) => {
  const formData = new FormData();
  formData.append("nik", process.env.ESIGN_NIK);
  formData.append("passphrase", process.env.ESIGN_PASSPHRASE);
  formData.append("file", file, {
    contentType: "application/pdf",
    filename: "sign.pdf",
  });
  formData.append("tampilan", "INVISIBLE");
  formData.append("jenis_response", "BASE64"); // karena ini hanyalah kebiasaan untuk mendapatkan file berupa base64
  return axios.post(`${process.env.ESIGN_API_URL}/api/sign/pdf`, formData);
};

// verifikasi dokumen yang telah ditandatangani
module.exports.verify = (file) => {
  // file harus bertipe buffer
  const formData = new FormData();
  formData.append("signed_file", file, {
    contentType: "application/pdf",
    filename: "document.pdf",
  });

  return axios.post(`${process.env.ESIGN_API_URL}/api/sign/verify`, formData);
};

// download signed dokumen
module.exports.download = (id_dokumen) => {
  return axios.get(
    `${process.env.ESIGN_API_URL}/api/sign/download/${id_dokumen}`
  );
};

// permohonan lupa passphrase
module.exports.forgetPassphrase = async (nik) => {
  return await axios.get(`${urlUserApi}/passphrase/forget/${nik}`);
};

// pendaftaran user
module.exports.register = (data) => {
  const {
    email,
    image_ttd,
    jabatan,
    kota,
    ktp,
    file,
    nama,
    nik,
    nip,
    nomor_telepon,
    provinsi,
    surat_rekomendasi,
    unit_kerja,
  } = data;

  const formData = new FormData();
  formData.append("email", email);
  formData.append("image_ttd", image_ttd);
  formData.append("jabatan", jabatan);
  formData.append("kota", kota);
  formData.append("ktp", ktp);
  formData.append("file", file);
  formData.append("nama", nama);
  formData.append("nik", nik);
  formData.append("nip", nip);
  formData.append("nomor_telepon", nomor_telepon);
  formData.append("provinsi", provinsi);
  formData.append("surat_rekomendasi", surat - surat_rekomendasi);
  formData.append("unit_kerja", unit_kerja);

  return axios.post(`${urlUserApi}/api/user/registrasi}`, formData);
};

// pengecekan status
module.exports.check = (nik) => {
  return axios.get(`${process.env.ESIGN_API_URL}/api/user/status/${nik}`);
};
