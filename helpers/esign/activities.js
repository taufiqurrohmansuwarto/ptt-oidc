const has = require("lodash/has");

// type login, request otp, sign, upload
module.exports.histories = async () => {};

// get user ip information
module.exports.ipInfo = (req) => {
  let userInfo;
  // for purpose testing only
  if (has(req.ipInfo, "error")) {
    userInfo = {
      ip_address: "localhost",
      useragent: req.useragent.source,
      time: new Date(),
      country: "ID",
      region: "JI",
      city: "Mojokerto",
      timezone: "Asia/Jakarta",
      lattitude: -7.4664,
      longitude: 112.4338,
    };
  } else {
    userInfo = {
      ip_address: req.ipInfo.ip,
      useragent: req.useragent.source,
      time: new Date(),
      country: req.ipInfo.country,
      region: req.ipInfo.region,
      timezone: req.ipInfo.timezone,
      city: req.ipInfo.city,
      lattitude: req.ipInfo.ll[0],
      longitude: req.ipInfo.ll[1],
    };
  }

  return userInfo;
};
