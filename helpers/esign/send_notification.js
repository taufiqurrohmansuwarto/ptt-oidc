const nodemailer = require("nodemailer");
const documentModel = require("../../models/siasn_esign/documents.model");
const recipientModel = require("../../models/siasn_esign/recipients.model");
const biodataModel = require("../../models/siasn_master/biodata.model");

//  you must test this shit with your own fuckin email : kaisardiemutbencong@gmail.com , ctrlxandctrlv@gmail.com, taufiqurrohman.suwarto@gmail.com,

/**
 *
 * @param {email} owner : owner adalah pemilik dokumen dengan status is_owner true pada list recipient
 * @param { email } listRecipient : list dari recipient yang ada pada proses dokumen upload
 * @param { rejected, approve } status : status dari recipient yang menyetujui atau yang membatalkan
 * @param { document_title, nip } title : judul beserta siapa yang membatalkan atau menyetujui dokumen pada esign
 */
module.exports.approvalNotificationEmail = async (props) => {
  const { status, docTitle, recipients, accountId, reason } = props;
  const recipientsWithoutCurrentUser = recipients.filter(
    (recipient) => parseInt(recipient.staff_id) !== parseInt(accountId)
  );

  const documentStatus = status === "rejected" ? "menolak" : "menyetujui";

  const transporter = nodemailer.createTransport({
    service: "gmail",
    auth: {
      user: process.env.EMAIL_USERNAME,
      pass: process.env.EMAIL_PASSWORD,
    },
  });

  let listPromises = [];
  recipientsWithoutCurrentUser.forEach(async (recipient) => {
    const mailOptions = {
      from: process.env.EMAIL_USERNAME,
      to: recipient.email,
      message: `<p>Halo ${recipient.user.nama} Kami ingin memberitahukan bahwa ${name} telah ${documentStatus} ${docTitle} karena ${reason}. Klik di sini untuk melihat dokumen tersebut, atau hubungi ${name} melalui diskusi.</p>`,
    };
    listPromises.push(transporter.sendMail(mailOptions));
  });
  await Promise.all(listPromises);
};

// venom bot in action
module.exports.approvalNotificationWhatsapp = async () => {};

// bayar
module.exports.approvalNotificationSMS = async () => {};

module.exports.recipientNotificationEmail = async (props) => {
  try {
    const { accountId, recipients, documentId } = props;
    const { title } = await documentModel.query().findById(documentId);
    const { nama } = await biodataModel.query().findById(accountId);

    const transporter = nodemailer.createTransport({
      service: "gmail",
      auth: {
        user: process.env.EMAIL_USERNAME,
        pass: process.env.EMAIL_PASSWORD,
      },
    });
    const recipientId = recipients.map((recipient) => recipient.staff_id);
    const currentListRecipients = await recipientModel
      .query()
      .whereIn("staff_id", recipientId)
      .andWhere("document_id", documentId)
      .withGraphFetched("user")
      .modifyGraph("user", (builder) => {
        builder.select("nama", "email");
      });

    let listPromises = [];
    currentListRecipients.forEach((recipient) => {
      const message = {
        from: process.env.EMAIL_USERNAME,
        to: recipient.user.email,
        subject: "Pemberitahuan document esign",
        html: `<p>Hallo ${recipient.user.nama}, ${nama} telah membagikan anda dokumen dengan judul ${title}, dan menjadikan anda sebagai ${recipient.role} </p>`,
      };
      listPromises.push(transporter.sendMail(message));
    });

    await Promise.all(listPromises);
  } catch (e) {
    console.log(e);
  }
};

const { parsePhoneNumberFromString } = require("libphonenumber-js");

module.exports.recipentNotificationWhatsapp = async (props) => {
  const { whatsAppClient, accountId, recipients, documentId } = props;
  const { title } = await documentModel.query().findById(documentId);
  const { nama } = await biodataModel.query().findById(accountId);

  const recipientId = recipients.map((recipient) => recipient.staff_id);
  const currentListRecipients = await recipientModel
    .query()
    .whereIn("staff_id", recipientId)
    .andWhere("document_id", documentId)
    .withGraphFetched("user")
    .modifyGraph("user", (builder) => {
      builder.select("nama", "no_hp");
    });

  let promises = [];
  currentListRecipients.forEach((recipient) => {
    const phoneNumber = parsePhoneNumberFromString(recipient.user.no_hp, "ID");
    const recipientNumber = `62${phoneNumber.nationalNumber}@c.us`;
    const message = `Hallo ${recipient.user.nama}, ${nama} telah membagikan anda dokumen dengan judul ${title}, dan menjadikan anda sebagai ${recipient.role}`;
    promises.push(whatsAppClient.sendText(recipientNumber, message));
  });

  await Promise.all(promises);
};

module.exports.recipientNotficationSMS = async (props) => {};
