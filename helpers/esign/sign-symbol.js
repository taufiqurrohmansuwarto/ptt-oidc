const { getEmpoyeeInformation } = require("../../helpers/get-account-detail");
const trim = require("lodash/trim");
const QrCode = require("qrcode");
const { createCanvas, Image } = require("canvas");
const userModel = require("../../models/siasn_master/biodata.model");

const magicWord = [
  "KEPALA",
  "ASISTEN",
  "CAMAT",
  "DEPUTI",
  "DIREKTUR",
  "DIRJEN",
  "INSPEKTUR",
  "LURAH",
  "SEKRETARIS",
  "WAKIL",
];

//  membuat stemple berdasarkan nama lengkap, jabatan, dan instansi
module.exports.draw = async (pegawaiId) => {
  const {
    nama,
    nip_baru,
    gelar_depan,
    gelar_belakang,
  } = await userModel
    .query()
    .where("pegawai_id", pegawaiId)
    .select("nip_baru", "nama", "gelar_depan", "gelar_belakang")
    .first();

  const namaPegawai = trim(`${gelar_depan} ${nama} ${gelar_belakang}`);
  const {
    pangkat,
    golongan,
    induk,
    instansi,
    jabatan,
    unit_kerja,
  } = await getEmpoyeeInformation(pegawaiId);

  const pegawai = {
    nama: namaPegawai,
    jabatan,
    nip: `NIP. ${nip_baru}`,
    pangkat,
    golongan,
    induk,
    instansi,
    unit_kerja,
  };

  const width = 350;
  const height = 175;

  const canvas = createCanvas(width, height);
  const context = canvas.getContext("2d");
  const pengantar = "Ditandantangi secara elektronik";

  const result = await QrCode.toDataURL(`${pegawai.instansi}_${pegawai.nip}`, {
    scale: 2,
  });

  const img = new Image();
  context.fillStyle = "rgba(255,255,255,0)";
  context.fillRect(0, 0, width, height);
  context.fillStyle = "#000";
  context.imageSmoothingEnabled = false;

  context.font = "bold 7pt Menlo";
  const measureText = context.measureText(pegawai.jabatan).width;
  const measureTextUnitKerja = context.measureText(pegawai.unit_kerja).width;

  img.src = result;
  const measureImage = context.measureText(img).width;
  const measurePengantar = context.measureText(pengantar).width;

  const [currentJabatan] = pegawai.jabatan.split(" ");
  if (magicWord.indexOf(currentJabatan) >= 0) {
    context.fillText(
      pegawai.jabatan,
      width / 2 - context.measureText(pegawai.jabatan).width / 2,
      height / 5
    );
    context.fillText(
      pegawai.unit_kerja,
      width / 2 - measureTextUnitKerja / 2,
      height / 3.6
    );
  } else {
    context.fillText(pegawai.jabatan, width / 2 - measureText / 2, height / 8);
    context.fillText(
      pegawai.unit_kerja,
      width / 2 - measureTextUnitKerja / 2,
      height / 5
    );
    context.fillText(
      pegawai.instansi,
      width / 2 - context.measureText(pegawai.instansi).width / 2,
      height / 3.6
    );
  }

  context.drawImage(img, width / 2 - measureImage / 2, height / 3);
  context.fillText(
    pengantar,
    width / 2 - measurePengantar / 2,
    height / 2 + 55
  );
  context.fillText(
    pegawai.nama,
    width / 2 - context.measureText(pegawai.nama).width / 2,
    height / 2 + 65
  );
  context.fillText(
    pegawai.pangkat,
    width / 2 - context.measureText(pegawai.pangkat).width / 2,
    height / 2 + 75
  );
  context.fillText(
    pegawai.nip,
    width / 2 - context.measureText(pegawai.nip).width / 2,
    height / 2 + 85
  );

  return canvas;
};
