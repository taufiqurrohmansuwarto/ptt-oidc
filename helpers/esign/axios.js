const axios = require("axios");
const FormData = require("form-data");

const auth = {
  username: process.env.ESIGN_USERNAME,
  password: process.env.ESIGN_PASSWORD,
};

axios.interceptors.request.use((config) => {
  if (config.data instanceof FormData) {
    Object.assign(config.headers, config.data.getHeaders());
  }
  Object.assign(config, { auth });
  return config;
});

module.exports = axios;
