module.exports.descriptionApprovalStatus = (
  listRecipients,
  workflow,
  type,
  accountId
) => {
  let description = null;
  // [error,info, success, warning]
  let descriptionStatus = null;

  const owner = listRecipients.find((recipient) => !!recipient.is_owner);

  const currentUser = listRecipients.find(
    (recipient) => parseInt(recipient.staff_id) === parseInt(accountId)
  );

  const selfSignUncompleted =
    workflow === "selfSign" && owner.status === "draft";
  const selfSignCompleted =
    workflow === "selfSign" && owner.status === "completed";

  const serialType = type === "serial";
  const paralelType = type === "parallel";

  // request from others
  const waitingSerial =
    serialType &&
    currentUser.signatory_status === "in progress" &&
    currentUser.status === "on progress";

  const serialRejected = serialType && owner.status === "rejected";

  const waitingSignSerial = waitingSerial && currentUser.role === "signer";
  const waitingReviewSerial = waitingSerial && currentUser.role === "reviewer";

  if (selfSignUncompleted) {
    description =
      "The document workflow is incomplete please sign the document at documents tab";
    descriptionStatus = "warning";
  } else if (
    serialType &&
    workflow === "requestFromOthers" &&
    owner.status === "completed"
  ) {
    descriptionStatus = "success";
    description = "The document workflow is completed";
  } else if (selfSignCompleted) {
    descriptionStatus = "success";
    description = "The document workflow is completed";
  } else if (
    serialType &&
    currentUser.role === "reviewer" &&
    currentUser.status === "completed" &&
    currentUser.signatory_status === "completed"
  ) {
    descriptionStatus = "success";
    description = "You already reviewed this document.";
  } else if (waitingSignSerial) {
    description = "This document is waiting for your sign";
    descriptionStatus = "info";
  } else if (waitingReviewSerial) {
    description = "This document is waiting for your reviewer";
    descriptionStatus = "info";
  } else if (serialRejected) {
    const rejectedUser =
      owner.status === "rejected" ? owner.rejected_user.nama : null;

    const rejectedReason = listRecipients.find(
      (recipient) =>
        parseInt(recipient.staff_id) === parseInt(owner.rejected_by)
    ).reason;

    description = `${rejectedUser} has rejected the document under reason "${rejectedReason}"`;
    descriptionStatus = "error";
  } else {
    description = "";
    descriptionStatus = "info";
  }

  return {
    description,
    descriptionStatus,
  };
};
