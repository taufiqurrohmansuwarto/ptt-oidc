/***
 * Dokumen dengan tipe serial artinya dokumen akan di lakukan transaksi request approval berdasarkan sequence, jika ada salah satu yang melakukan reject dan dia berada di tengah maka antrian yang ada dibawahnya otomatis akan melakukan reject beserta owner dari dokumen itu dengan menuliskan siapa yang melakukan reject.
 */
const documentModel = require("../../models/siasn_esign/documents.model");
const historyModel = require("../../models/siasn_esign/document_histories.model");
const otpModel = require("../../models/siasn_esign/otp.model");

const { totp } = require("otplib");
const { rejectDocument, sign } = require("../../helpers/esign/sign");

const recipientModel = require("../../models/siasn_esign/recipients.model");

const ownerAndNotOwner = (listRecipient) => {
  const owner = listRecipient.find((recipient) => !!recipient.is_owner);
  const recipients = listRecipient.filter(
    (recipient) => !recipient.is_owner && recipient.role !== null
  );

  return {
    owner,
    recipients,
  };
};

module.exports.documentApproveSign = async (
  res,
  trx,
  reason = "i agree to this document",
  document,
  listRecipient,
  currentUser,
  historyInformation,
  documentData,
  otpCode
) => {
  try {
    const { owner, recipients } = ownerAndNotOwner(listRecipient);
    const { documentId, documentWorkflow } = document;

    const { client } = documentData;
    const upload = (...args) => {
      return new Promise((resolve, reject) => {
        client.putObject(...args, function (err) {
          if (err) {
            reject(err);
          } else {
            resolve(true);
          }
        });
      });
    };

    if (documentWorkflow === "selfSign") {
      return res.status(403).json({ code: 403, message: "Wait" });
    }

    if (owner.status !== "on progress") {
      return res.status(403).json({
        code: 403,
        message:
          "Cannot approve sign because document status is already completed or rejected",
      });
    }

    if (currentUser.role !== "signer") {
      return res.status(403).json({
        code: 403,
        message: "Cannot sign this document because your role is not signer",
      });
    }

    // const { secret } = await otpModel
    //   .query()
    //   .where("document_id", documentId)
    //   .andWhere("staff_id", currentUser.staff_id)
    //   .orderBy("created_at", "desc")
    //   .first();

    // const isValid = totp.check(otpCode.toString(), secret);

    // if (!secret || !isValid) {
    //   return res.status(403).json({ code: 403, message: "Invalid otp code" });
    // }

    if (currentUser.sequence === 1 && recipients.length === 1) {
      const { pdfBytes, documentSignName } = await sign(documentData);

      await historyModel.query().insert(historyInformation);
      await recipientModel
        .query()
        .patch({
          reason,
          signatory_status: "completed",
          status: "completed",
          is_approve: true,
          approval_date: new Date(),
          sign_pages: documentData.signCoordinate.map((d) => d.page),
          total_sign_pages: documentData.signCoordinate.length,
          ip_address: historyInformation.ip_address,
          device: historyInformation.useragent,
        })
        .where("id", currentUser.id);

      await upload("esign", documentSignName, Buffer.from(pdfBytes));

      await documentModel
        .query()
        .patch({ sign_document: documentSignName })
        .where("id", documentId);

      await recipientModel
        .query()
        .patch({
          status: "completed",
        })
        .where("id", owner.id);

      return res.status(200).json({
        code: 200,
        message: "successfully sign the document",
        data: {
          id: documentId,
          date: new Date(),
          status: "approved sign",
          reason,
        },
      });
      // jika dia yang terakhir
    } else if (currentUser.sequence === recipients.length) {
      const beforeUser = await recipientModel
        .query()
        .where("document_id", documentId)
        .withGraphFetched("[user]")
        .modifyGraph("[user]", (builder) => {
          builder.select("nama");
        })
        .andWhere("sequence", currentUser.sequence - 1)
        .andWhereNot("signatory_status", "completed")
        .andWhereNot("status", "completed")
        .first();

      if (!beforeUser) {
        const { pdfBytes, documentSignName } = await sign(documentData);

        await historyModel.query().insert(historyInformation);
        await recipientModel
          .query()
          .patch({
            reason,
            signatory_status: "completed",
            status: "completed",
            is_approve: true,
            approval_date: new Date(),
            sign_pages: documentData.signCoordinate.map((d) => d.page),
            total_sign_pages: documentData.signCoordinate.length,
            ip_address: historyInformation.ip_address,
            device: historyInformation.useragent,
          })
          .where("id", currentUser.id);

        await upload("esign", documentSignName, Buffer.from(pdfBytes));

        await documentModel
          .query()
          .patch({ sign_document: documentSignName })
          .where("id", documentId);

        await recipientModel
          .query()
          .patch({
            status: "completed",
          })
          .where("id", owner.id);

        return res.status(200).json({
          code: 200,
          message: "successfully sign the document",
          data: {
            id: documentId,
            date: new Date(),
            status: "approved sign",
            reason,
          },
        });
      } else {
        return res.statu(403).json({
          code: 403,
          message: `This document type is serial. Please wait ${beforeUser.user.nama} to ${beforeUser.role}`,
        });
      }
    } else if (recipients.length != 1) {
      const beforeUser = await recipientModel
        .query()
        .where("document_id", documentId)
        .withGraphFetched("[user]")
        .modifyGraph("[user]", (builder) => {
          builder.select("nama");
        })
        .andWhere("sequence", currentUser.sequence - 1)
        .andWhereNot("signatory_status", "completed")
        .andWhereNot("status", "completed")
        .first();

      if (!beforeUser) {
        const { pdfBytes, documentSignName } = await sign(documentData);

        await historyModel.query().insert(historyInformation);
        await recipientModel
          .query()
          .patch({
            reason,
            signatory_status: "completed",
            status: "completed",
            is_approve: true,
            approval_date: new Date(),
            sign_pages: documentData.signCoordinate.map((d) => d.page),
            total_sign_pages: documentData.signCoordinate.length,
            ip_address: historyInformation.ip_address,
            device: historyInformation.useragent,
          })
          .where("id", currentUser.id);

        await upload("esign", documentSignName, Buffer.from(pdfBytes));

        await documentModel
          .query()
          .patch({ ongoing_document: documentSignName })
          .where("id", documentId);

        return res.status(200).json({
          code: 200,
          message: "successfully sign the document",
          data: {
            id: documentId,
            date: new Date(),
            status: "approved sign",
            reason,
          },
        });
      } else {
        return res.status(403).json({
          code: 403,
          message: `This document is type serial. Please wait ${beforeUser.user.nama} to ${beforeUser.role}`,
          data: {
            id: documentId,
          },
        });
      }
    }
  } catch (e) {
    console.log(e);
    return res.status(400).json({ message: "Internal Server Error" });
  }
};

module.exports.approveReviewer = async (
  res,
  trx,
  reason = "i disagree to this document",
  document,
  listRecipient,
  currentUser,
  historyInformation
) => {
  try {
    const { owner, recipients } = ownerAndNotOwner(listRecipient);
    const { documentId, documentWorkflow } = document;

    // selfsign cannot use approval reviewer
    if (documentWorkflow === "selfSign") {
      return res.status(403).json({
        code: 403,
        message:
          "document workflow allowed only request from others or sign and request",
      });
    }

    // only sstatus with on progresss
    if (owner.status !== "on progress") {
      return res.status(403).json({
        code: 403,
        message: "document status is already completed or rejected",
      });
    }

    // melakukan pengecekan. jika dia berurutan 1 dan recipient nya cuman dia, update dia beserta ownernya. jika dia di urutan ke dua cek dulu atasnya apa sudah melakukan approval
    if (currentUser.sequence === 1 && recipients.length === 1) {
      // update the currentuser
      await recipientModel
        .query()
        .patch({
          reason,
          signatory_status: "completed",
          is_approve: true,
          status: "completed",
          approval_date: new Date(),
        })
        .where("id", currentUser.id);
      // update the user
      await recipientModel
        .query()
        .patch({ status: "completed" })
        .where("id", owner.id);
      // update the history
      await historyModel.query().insert(historyInformation);

      return res
        .status(200)
        .json({ code: 200, message: "successfully reviewed the document" });
    } else if (currentUser.sequence === recipients.length) {
      const beforeUser = await recipientModel
        .query()
        .where("document_id", documentId)
        .withGraphFetched("[user]")
        .modifyGraph("[user]", (builder) => {
          builder.select("nama");
        })
        .andWhere("sequence", currentUser.sequence - 1)
        .andWhereNot("signatory_status", "completed")
        .andWhereNot("status", "completed")
        .first();

      if (!beforeUser) {
        // update the currentuser
        await recipientModel
          .query()
          .patch({
            reason,
            signatory_status: "completed",
            is_approve: true,
            status: "completed",
            approval_date: new Date(),
          })
          .where("id", currentUser.id);
        // update the user
        await recipientModel
          .query()
          .patch({ status: "completed" })
          .where("id", owner.id);

        await historyModel.query().insert(historyInformation);
        return res.status(200).json({
          code: 200,
          message: "successfully reviewed the document",
          data: {
            id: documentId,
            reason,
            signatory_status: "completed",
          },
        });
      } else {
        return res.status(403).json({
          code: 403,
          message: `This document type is serial. Please wait ${beforeUser.user.nama} to ${beforeUser.role}`,
        });
      }
    } else if (recipients.length !== 1) {
      const beforeUser = await recipientModel
        .query()
        .where("document_id", documentId)
        .withGraphFetched("[user]")
        .modifyGraph("[user]", (builder) => {
          builder.select("nama");
        })
        .andWhere("sequence", currentUser.sequence - 1)
        .andWhereNot("signatory_status", "completed")
        .andWhereNot("status", "completed")
        .first();

      if (!beforeUser) {
        // update the currentuser
        await recipientModel
          .query()
          .patch({
            reason,
            signatory_status: "completed",
            is_approve: true,
            status: "completed",
            approval_date: new Date(),
          })
          .where("id", currentUser.id);
        // update the user

        await historyModel.query().insert(historyInformation);
        return res.status(200).json({
          code: 200,
          message: "successfully reviewed the document",
          data: {
            id: documentId,
          },
        });
      } else {
        return res.status(403).json({
          code: 403,
          message: `This document type is serial. Please wait ${beforeUser.user.nama} to ${beforeUser.role}`,
          data: {
            id: documentId,
          },
        });
      }
    }
  } catch (e) {
    console.log(e);
    return res
      .status(400)
      .json({ code: 400, message: "Internal Server Error" });
  }
};

const {
  approvalNotificationEmail,
} = require("../../helpers/esign/send_notification");

module.exports.rejectReviewer = async (
  res,
  trx,
  reason = "i disagree to this document",
  document,
  listRecipient,
  currentUser,
  historyInformation,
  documentData
) => {
  try {
    const { owner, recipients } = ownerAndNotOwner(listRecipient);
    const { documentId, documentWorkflow } = document;

    const { client } = documentData;
    const upload = (...args) => {
      return new Promise((resolve, reject) => {
        client.putObject(...args, function (err) {
          if (err) {
            reject(err);
          } else {
            resolve(true);
          }
        });
      });
    };

    // selfsign cannot use approval reviewer
    if (documentWorkflow === "selfSign") {
      return res.status(403).json({
        code: 403,
        message:
          "document workflow only allowed request from others or sign and request",
      });
    }

    // only sstatus with on progresss
    if (owner.status !== "on progress") {
      return res.status(403).json({
        code: 403,
        message: "document status is already completed or rejected",
      });
    }

    // melakukan pengecekan. jika dia berurutan 1 dan recipient nya cuman dia, update dia beserta ownernya. jika dia di urutan ke dua cek dulu atasnya apa sudah melakukan approval
    if (currentUser.sequence === 1 && recipients.length === 1) {
      // update the currentuser
      await recipientModel
        .query()
        .patch({
          reason,
          signatory_status: "rejected",
          is_approve: true,
          status: "rejected",
          approval_date: new Date(),
        })
        .where("id", currentUser.id);

      // update the user
      await recipientModel
        .query()
        .patch({
          status: "rejected",
          rejected_by: currentUser.staff_id,
          rejected_reason: reason,
        })
        .where("id", owner.id);

      // update the history
      await historyModel.query().insert(historyInformation);

      // notification
      await approvalNotificationEmail({
        status: "rejected",
        documentTitle: "",
        recipients,
        accountId: currentUser.staff_id,
        reason,
      });

      const { pdfBytes, documentSignName } = await rejectDocument(documentData);
      await upload("esign", documentSignName, Buffer.from(pdfBytes));
      await documentModel
        .query()
        .patch({ sign_document: documentSignName })
        .where("id", documentId);

      return res.status(200).json({
        code: 200,
        message: "successfully rejected the document",
        data: {
          id: documentId,
          date: new Date(),
          status: "rejected",
          reason,
        },
      });
      // berarti dia terakhir
    } else if (currentUser.sequence === recipients.length) {
      const beforeUser = await recipientModel
        .query()
        .where("document_id", documentId)
        .withGraphFetched("[user]")
        .modifyGraph("[user]", (builder) => {
          builder.select("nama");
        })
        .andWhere("sequence", currentUser.sequence - 1)
        .andWhere("signatory_status", "in progress")
        .andWhere("status", "on progress")
        .first();

      if (!beforeUser) {
        // update the currentuser
        await recipientModel
          .query()
          .patch({
            reason,
            signatory_status: "rejected",
            is_approve: true,
            status: "rejected",
            approval_date: new Date(),
          })
          .where("id", currentUser.id);

        // update the user
        await recipientModel
          .query()
          .patch({
            status: "rejected",
            rejected_by: currentUser.staff_id,
            rejected_reason: reason,
          })
          .where("id", owner.id);

        await historyModel.query().insert(historyInformation);
        const { pdfBytes, documentSignName } = await rejectDocument(
          documentData
        );
        await upload("esign", documentSignName, Buffer.from(pdfBytes));
        await documentModel
          .query()
          .patch({ sign_document: documentSignName })
          .where("id", documentId);

        return res.status(200).json({
          code: 200,
          message: "successfully rejected the document",
          data: {
            id: documentId,
            date: new Date(),
            status: "rejected",
            reason,
          },
        });
      } else {
        return res.status(403).json({
          code: 403,
          message: `This document type is serial. Please wait ${beforeUser.user.nama} to ${beforeUser.role}`,
        });
      }
    } else if (recipients.length !== 1) {
      const beforeUser = await recipientModel
        .query()
        .where("document_id", documentId)
        .withGraphFetched("[user]")
        .modifyGraph("[user]", (builder) => {
          builder.select("nama");
        })
        .andWhere("sequence", currentUser.sequence - 1)
        .andWhere("signatory_status", "in progress")
        .andWhere("status", "on progress")
        .first();

      const afterUser = await recipientModel
        .query()
        .where("document_id", documentId)
        .andWhere("sequence", ">", currentUser.sequence)
        .andWhere("signatory_status", "in progress")
        .andWhere("status", "on progress");

      if (!beforeUser) {
        if (afterUser.length === 0) {
          await recipientModel
            .query()
            .patch({
              reason,
              signatory_status: "rejected",
              is_approve: true,
              status: "rejected",
              approval_date: new Date(),
            })
            .where("id", currentUser.id);

          await recipientModel
            .query()
            .patch({
              status: "rejected",
              rejected_by: currentUser.staff_id,
              rejected_reason: reason,
            })
            .where("id", owner.id);

          await historyModel.query().insert(historyInformation);
          const { pdfBytes, documentSignName } = await rejectDocument(
            documentData
          );

          await upload("esign", documentSignName, Buffer.from(pdfBytes));

          await documentModel
            .query()
            .patch({ sign_document: documentSignName })
            .where("id", documentId);
          return res.status(200).json({
            code: 200,
            message: "successfully reviewed the document",
            data: {
              id: documentId,
              data: {
                id: documentId,
                status: "rejected",
                reason,
                date: new Date(),
              },
            },
          });
        } else {
          // update awake dewe
          await recipientModel
            .query()
            .patch({
              signatory_status: "rejected",
              reason,
              is_approve: true,
              status: "rejected",
              approval_date: new Date(),
            })
            .where("id", currentUser.id);

          // update the list
          const afterUserId = afterUser.map((user) => user.id);
          await recipientModel
            .query()
            .patch({
              status: "rejected",
              rejected_by: currentUser.staff_id,
              rejected_reason: reason,
            })
            .whereIn("id", afterUserId)
            .orWhere("id", owner.id);

          await historyModel.query().insert(historyInformation);
          const { pdfBytes, documentSignName } = await rejectDocument(
            documentData
          );
          await upload("esign", documentSignName, Buffer.from(pdfBytes));

          return res.status(200).json({
            code: 200,
            message: "successfully reviewed the document",
            data: {
              id: documentId,
              data: {
                id: documentId,
                status: "rejected",
                reason,
                date: new Date(),
              },
            },
          });
        }
      } else {
        return res.status(403).json({
          code: 403,
          message: `This document type is serial. Please wait ${beforeUser.user.nama} to ${beforeUser.role}`,
          data: {
            id: documentId,
          },
        });
      }
    }
  } catch (e) {
    console.log(e);
    return res
      .status(400)
      .json({ code: 400, message: "Internal Server Error" });
  }
};
