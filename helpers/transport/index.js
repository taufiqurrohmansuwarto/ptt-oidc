const nodemailer = require("nodemailer");
const { parsePhoneNumberFromString } = require("libphonenumber-js");

module.exports.sendEmailOTP = async (user, token, type = "OTP") => {
  const { email, nama } = user;
  const transporter = nodemailer.createTransport({
    service: "gmail",
    auth: {
      user: process.env.EMAIL_USERNAME,
      pass: process.env.EMAIL_PASSWORD,
    },
  });

  const mailOptions = {
    from: process.env.EMAIL_USERNAME,
    to: email,
  };

  if (type === "OTP") {
    mailOptions = {
      ...mailOptions,
      subject: "Kode OTP Esign",
      html: `<h1>
     Hallo, ${nama} ini adalah token rahasia untuk melakukan persetujuan pada aplikasi esign. Token kamu adalah adalah ${token} 
    </h1>`,
    };

    return transporter.sendMail(mailOptions);
  }
};

module.exports.sendWhatsappOTP = (client, user, token, type = "OTP") => {
  const { no_hp, nama } = user;
  const phoneNumber = `62${
    parsePhoneNumberFromString(no_hp, "ID").nationalNumber
  }@c.us`;

  let message;
  if (type === "OTP") {
    message = `Hallo ${nama} ini adalah token rahasia untuk melakukan persetujuan pada aplikasi esign. Token kamu adalah ${token}`;
    return client.sendText(phoneNumber, message);
  }
};
