const historiesModel = require("../../../models/siasn_esign/document_histories.model");
const documentModel = require("../../../models/siasn_esign/documents.model");
const {
  serializeHistory,
} = require("../../../helpers/esign/serialize_document");

// activities all users
module.exports.activities = async (req, res) => {
  try {
    const { accountId } = res.locals.token;
    const page = req.query.page || 0;
    const pageSize = req.query.pageSize || 10;

    // activities
    const { total, results } = await historiesModel
      .query()
      .where("staff_id", accountId)
      .withGraphFetched("document")
      .modifyGraph("[document]", (builder) => {
        builder.select("id", "title");
      })
      .orderBy("created_at", "desc")
      .page(page, pageSize);

    return res
      .status(200)
      .json({ code: 200, message: "success", data: { total, list: results } });
  } catch (e) {
    return res
      .status(400)
      .json({ code: 400, message: "Internal Server Error" });
  }
};

module.exports.historyDocument = async (req, res) => {
  try {
    const { accountId } = res.locals.token;
    const { documentId } = req.params;

    const page = req.query.page || 0;
    const pageSize = req.query.pageSize || 10;

    const document = await documentModel.query().findById(documentId);

    // todo harus cari di recipientnya

    if (!document) {
      return res.status(404).json({ code: 404, message: "document not found" });
    }

    const { total, results } = await historiesModel
      .query()
      .select("id", "document_id", "type", "action", "staff_id", "created_at")
      // .where("staff_id", accountId)
      .andWhere("document_id", documentId)
      .withGraphFetched("[user.[fileDiri], document]")
      .modifyGraph("user", (builder) => {
        builder.select("nip_baru", "nama");
      })
      .modifyGraph("[user.[fileDiri]]", (builder) => {
        builder.select("file_foto");
      })
      .modifyGraph("[document]", (builder) => {
        builder.select("id", "uploader");
      })
      .orderBy("created_at", "desc")
      .page(page, pageSize);

    return res.status(200).json({
      code: 200,
      message: "success",
      data: {
        total,
        list: serializeHistory(document.uploader, results),
      },
    });
  } catch (e) {
    console.log(e);
    return res
      .status(400)
      .json({ code: 400, message: "Internal Server Error" });
  }
};
