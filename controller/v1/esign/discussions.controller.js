const discussionsModel = require("../../../models/siasn_esign/discussions.model");
const documentModel = require("../../../models/siasn_esign/documents.model");
const recipientModel = require("../../../models/siasn_esign/recipients.model");

const { arrayToTree } = require("performant-array-to-tree");

const serializeDataDiscussion = (discussions, ownerId) => {
  if (discussions.length === 0) {
    return [];
  } else {
    return discussions.map((discussion) => ({
      id: discussion.id,
      parent_id: discussion.parent_id,
      is_owner: discussion.user.pegawai_id === ownerId,
      message: discussion.message,
      document_id: discussion.document_id,
      name: discussion.user.nama,
      created_at: discussion.created_at,
      nip: discussion.user.nip_baru,
      profile_picture: `https://master.bkd.jatimprov.go.id/files_jatimprov/${discussion.user.fileDiri.file_foto}`,
    }));
  }
};

module.exports.index = async (req, res) => {
  try {
    const { documentId } = req.params;
    const currentDocument = await documentModel.query().findById(documentId);
    if (!currentDocument) {
      return res.status(404).json({ code: 404, message: "document not found" });
    }

    const { staff_id: ownerId } = await recipientModel
      .query()
      .where("document_id", documentId)
      .andWhere("is_owner", true)
      .first();

    const currentDiscussions = await discussionsModel
      .query()
      .where("document_id", currentDocument.id)
      .withGraphFetched("[user.[fileDiri]]")
      .modifyGraph("[user]", (builder) => {
        builder.select("nip_baru", "nama", "pegawai_id");
      })
      .modifyGraph("[user.[fileDiri]]", (builder) => {
        builder.select("file_foto");
      })
      .orderBy("created_at", "desc");

    //   serialize data

    return res.status(200).json({
      code: 200,
      message: "success",
      data: arrayToTree(serializeDataDiscussion(currentDiscussions, ownerId), {
        parentId: "parent_id",
        dataField: null,
      }),
    });
  } catch (e) {
    return res.status(400).json({
      code: 400,
      message: "Internal Server Error",
    });
  }
};

module.exports.create = async (req, res) => {
  try {
    const { accountId } = res.locals.token;
    const { documentId } = req.params;
    const { parent_id, message } = req.body;

    const currentDocument = await documentModel.query().findById(documentId);

    if (!currentDocument) {
      return res.status(404).json({ code: 404, message: "document not found" });
    }

    const data = {
      document_id: currentDocument.id,
      staff_id: accountId,
      message,
      parent_id: parent_id ? parent_id : null,
    };

    const discussions = await discussionsModel
      .query()
      .insertAndFetch(data)
      .select("id", "document_id", "parent_id", "message", "staff_id")
      .withGraphFetched("[user.[fileDiri]]")
      .modifyGraph("[user]", (builder) => {
        builder.select("nama", "nip_baru");
      })
      .modifyGraph("[user.[fileDiri]]", (builder) => {
        builder.select("file_foto");
      });

    return res.status(200).json({
      code: 200,
      message: "success",
      data: {
        id: discussions.id,
        document_id: discussions.document_id,
        parent_id: discussions.parent_id,
        staff_id: discussions.staff_id,
        name: discussions.user.nama,
        nip: discussions.user.nip,
        profile_picture: `https://master.bkd.jatimprov.go.id/files_jatimprov/${discussions.user.fileDiri.file_foto}`,
      },
    });
  } catch (e) {
    return res
      .status(400)
      .json({ code: 400, message: "Internal Server Error" });
  }
};
