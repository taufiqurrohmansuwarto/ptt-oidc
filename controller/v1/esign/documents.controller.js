const { totp } = require("otplib");
totp.options = { step: 120 };

const documentModel = require("../../../models/siasn_esign/documents.model");
const recipientsModel = require("../../../models/siasn_esign/recipients.model");

const { ipInfo } = require("../../../helpers/esign/activities");
const historiesModel = require("../../../models/siasn_esign/document_histories.model");

const {
  serializeDocument,
} = require("../../../helpers/esign/serialize_document");

module.exports.download = async (req, res) => {
  try {
    const { documentId } = req.params;
    const client = req.app.locals.minioClient;

    const document = await documentModel
      .query()
      .findById(documentId)
      .withGraphFetched("recipients")
      .modifyGraph("recipients", (builder) => {
        builder.where("is_owner", true).select("staff_id", "status");
      });

    if (!document) {
      return res
        .status(404)
        .json({ code: 4040, message: "document not found" });
    }

    // pasti ada owner dari recipients
    const [data] = document.recipients;

    const download = (...args) => {
      return new Promise((resolve, reject) => {
        client.getObject(...args, function (err, stream) {
          if (err) {
            reject(err);
          } else {
            let buffers = [];
            stream.on("data", function (chunk) {
              buffers.push(chunk);
            });
            stream.on("end", function () {
              const buffer = Buffer.concat(buffers);
              resolve(buffer);
            });
            stream.on("error", function (err) {
              reject(err);
            });
          }
        });
      });
    };

    let buffer;
    if (data.status === "draft" || data.status === "in progress") {
      buffer = await download("esign", document.initial_document);
    } else {
      buffer = await download("esign", docuemnt.sign_document);
    }

    res.writeHead(200, {
      "Content-Disposition": "attachment;filename=" + document.title + ".pdf",
      "Content-Type": "application/pdf",
    });
    res.end(Buffer.from(buffer, "base64"));
  } catch (e) {
    return res
      .status(400)
      .json({ code: 400, message: "Internal Server Error" });
  }
};

module.exports.getDetails = async (req, res) => {
  try {
    const { documentId } = req.params;
    const { accountId } = res.locals.token;
    const client = req.app.locals.minioClient;

    const document = await documentModel
      .query()
      .findById(documentId)
      .withGraphFetched("[recipients,owner.[fileDiri]]")
      .modifyGraph("[owner]", (builder) => {
        builder.select("nama", "nip_baru");
      })
      .modifyGraph("owner.[fileDiri]", (builder) => {
        builder.select("file_foto");
      })
      .modifyGraph("[recipients]", (builder) => {
        builder.select(
          "id",
          "staff_id",
          "role",
          "signatory_status",
          "is_owner",
          "status"
        );
      });

    if (!document) {
      return res.status(404).json({ code: 404, message: "document not found" });
    }

    const preSign = (...args) => {
      return new Promise((resolve, reject) => {
        client.presignedGetObject(...args, function (err, presignedUrl) {
          if (err) {
            reject(err);
          } else {
            resolve(presignedUrl);
          }
        });
      });
    };

    const ownerStatus = document.recipients.find(
      (recipient) => !!recipient.is_owner
    ).status;

    let initial_document = null;
    let sign_document = null;

    if (document.initial_document) {
      const result = await preSign(
        "esign",
        document.initial_document,
        1 * 60 * 60
      );
      initial_document = {
        download: result,
        expired_date: new Date(),
      };
    }

    if (document.sign_document) {
      const result = await preSign(
        "esign",
        document.sign_document,
        1 * 60 * 60
      );

      sign_document = {
        download: result,
        expired_date: new Date(),
      };
    }

    // sign information
    const signInformation = await recipientsModel
      .query()
      .select(
        "sign_pages",
        "total_sign_pages",
        "sign_coordinate",
        "id",
        "ip_address",
        "device",
        "approval_date",
        "reason"
      )
      .where("document_id", documentId)
      .andWhere("staff_id", accountId)
      .andWhere("role", "signer")
      .andWhere("is_approve", true);

    const data = {
      id: document.id,
      type: document.type,
      title: document.title,
      workflow: document.workflow,
      type: document.type,
      document_pages: document.document_pages,
      upload_date: document.upload_date,
      owner: document.owner.nama,
      nip: document.owner.nip_baru,
      status: ownerStatus,
      recipients: document.recipients,
      profile_picture: `https://master.bkd.jatimprov.go.id/files_jatimprov/${document.owner.fileDiri.file_foto}`,
      initial_document,
      sign_document,
      sign_information: signInformation,
    };

    const ipInformation = ipInfo(req);
    const dataHistories = {
      ...ipInformation,
      staff_id: res.locals.token.accountId,
      document_id: document.id,
      type: "document",
      action: "opened",
    };

    await historiesModel.query().insert(dataHistories);
    return res.status(200).json({ code: 200, message: "success", data });
  } catch (e) {
    return res
      .status(400)
      .json({ code: 400, message: "Internal Server Error" });
  }
};

module.exports.archieved = async (req, res) => {
  try {
    const { documentId } = req.params;
    const { accountId } = res.locals.token;

    const document = await recipientsModel
      .query()
      .where("document_id", documentId)
      .andWhere("staff_id", accountId)
      .andWhere("status", "completed")
      .first();

    if (!document) {
      return res.status(404).json({ code: 404, message: "document not found" });
    } else {
      await recipientsModel
        .query()
        .where("document_id", documentId)
        .andWhere("staff_id", accountId)
        .andWhere("status", "completed")
        .patch({
          is_archieved: true,
        });

      return res
        .status(200)
        .json({ code: 200, message: "success archieved document" });
    }
  } catch (e) {
    console.log(e);
    return res
      .status(400)
      .json({ code: 400, message: "Internal Server Error" });
  }
};

module.exports.delete = async (req, res) => {
  const trx = await documentModel.startTransaction();
  try {
    const { documentId } = req.params;
    const { accountId } = res.locals.token;
    const client = req.app.locals.minioClient;

    const removeObjectWithPromise = (...args) => {
      return new Promise((resolve, reject) => {
        client.removeObject(...args, function (err) {
          if (err) {
            reject(err);
          } else {
            resolve();
          }
        });
      });
    };

    // check the owner
    const checkDocument = await recipientsModel
      .query()
      .where("document_id", documentId)
      .andWhere("staff_id", accountId)
      .andWhere("is_owner", true)
      .andWhere("status", "draft");

    if (Array.isArray(checkDocument)) {
      if (checkDocument.length === 0) {
        return res.status(403).json({
          code: 403,
          message:
            "cant remove this document. Document is not belongs to you or document status is not draft",
        });
      } else {
        await documentModel.query(trx).deleteById(documentId);
        await recipientsModel
          .query(trx)
          .delete()
          .where("document_id", documentId);
        await historiesModel
          .query(trx)
          .delete()
          .where("document_id", documentId);
        await removeObjectWithPromise("esign", documentId);
        await trx.commit();
        return res
          .status(200)
          .json({ code: 200, message: "successfully deleted" });
      }
    }
  } catch (e) {
    await trx.rollback();
    return res
      .status(400)
      .json({ code: 400, message: "Internal Server Error" });
  }
};

// the some of filter
module.exports.index = async (req, res) => {
  try {
    const { accountId } = res.locals.token;

    const type = req.query.type || "all";
    const page = req.query.page || 0;
    const pageSize = req.query.pageSize || 10;

    const data = await recipientsModel
      .query()
      .select(
        "siasn_esign.recipients.id",
        "siasn_esign.recipients.staff_id",
        "siasn_esign.recipients.document_id",
        "siasn_esign.recipients.status",
        "siasn_esign.recipients.created_at",
        "siasn_esign.recipients.signatory_status",
        "siasn_esign.recipients.role"
      )
      .where((builder) => {
        if (type === "rejected") {
          builder
            .select("siasn_esign.recipients.staff_id", accountId)
            .andWhere("is_owner", true)
            .andWhere("status", "rejected");
        } else if (type === "waiting") {
          builder
            .select("siasn_esign.recipients.staff_id", accountId)
            .andWhereNot("role", null)
            .andWhere("signatory_status", "in progress")
            .andWhere("status", "on progress");
        } else if (type === "all") {
          builder
            .where("siasn_esign.recipients.staff_id", accountId)
            .andWhere("is_archieved", false);
        } else if (type === "draft") {
          builder
            .where("siasn_esign.recipients.status", "draft")
            .andWhere("siasn_esign.recipients.staff_id", accountId);
        } else if (type === "completed") {
          builder
            .where("siasn_esign.recipients.status", "completed")
            .andWhere("siasn_esign.recipients.staff_id", accountId);
        } else if (type === "archieved") {
          builder
            .where("siasn_esign.recipients.status", "completed")
            .andWhere("siasn_esign.recipients.is_archieved", true)
            .andWhere("siasn_esign.recipients.staff_id", accountId);
        }
      })
      .andWhere((builder) => {
        if (req.query.title) {
          builder.where(
            "siasn_esign.recipients.document_title",
            "like",
            `%${req.query.title}%`
          );
        }
      })
      // .withGraphJoined("document")
      .withGraphFetched(
        "[document.[recipients.[user.[fileDiri]], owner.[fileDiri]]]"
      )
      .modifyGraph("[document.[recipients]]", (builder) => {
        builder
          .select(
            "staff_id",
            "role",
            "signatory_status",
            "id",
            "status",
            "is_approve"
          )
          .whereNot("role", null)
          .orderBy("sequence", "asc");
      })
      .modifyGraph("[document.[recipients.[user]]]", (builder) => {
        builder.select("nama", "nip_baru");
      })
      .modifyGraph("[document.[recipients.[user.[fileDiri]]]]", (builder) => {
        builder.select("file_foto");
      })
      .modifyGraph("[document.[owner]]", (builder) => {
        builder.select("nama", "nip_baru");
      })
      .modifyGraph("[document.[owner.[fileDiri]]]", (builder) => {
        builder.select("file_foto");
      })
      .limit(pageSize)
      .offset(page * pageSize)
      .orderBy("recipients.upload_date", "desc");

    const totalQuery = await recipientsModel
      .query()
      .select(
        "siasn_esign.recipients.id",
        "siasn_esign.recipients.staff_id",
        "siasn_esign.recipients.document_id",
        "siasn_esign.recipients.status"
      )
      .where((builder) => {
        if (type === "rejected") {
          builder
            .select("siasn_esign.recipients.staff_id", accountId)
            .andWhere("is_owner", true)
            .andWhere("status", "rejected");
        } else if (type === "waiting") {
          builder
            .select("siasn_esign.recipients.staff_id", accountId)
            .andWhereNot("role", null)
            .andWhere("signatory_status", "in progress")
            .andWhere("status", "on progress");
        } else if (type === "all") {
          builder
            .where("siasn_esign.recipients.staff_id", accountId)
            .andWhere("is_archieved", false);
        } else if (type === "draft") {
          builder
            .where("siasn_esign.recipients.status", "draft")
            .andWhere("siasn_esign.recipients.staff_id", accountId);
        } else if (type === "completed") {
          builder
            .where("siasn_esign.recipients.status", "completed")
            .andWhere("siasn_esign.recipients.staff_id", accountId);
        } else if (type === "archieved") {
          builder
            .where("siasn_esign.recipients.status", "completed")
            .andWhere("siasn_esign.recipients.is_archieved", true)
            .andWhere("siasn_esign.recipients.staff_id", accountId);
        }
      })
      .andWhere((builder) => {
        if (req.query.title) {
          builder.where(
            "siasn_esign.recipients.document_title",
            "like",
            `%${req.query.title}%`
          );
        }
      })
      .count("siasn_esign.recipients.id");

    const total =
      totalQuery.length === 0
        ? 0
        : totalQuery[0]["count(`siasn_esign`.`recipients`.`id`)"];

    return res.status(200).json({
      meta: { total: total, page, pageSize },
      data: {
        total: total,
        list: serializeDocument(accountId, data),
      },
      code: 200,
      message: "success",
    });
  } catch (e) {
    console.log(e);
    return res
      .status(400)
      .json({ code: 400, meta: {}, message: "Internal server Error" });
  }
};
