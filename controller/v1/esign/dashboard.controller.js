const recipientModel = require("../../../models/siasn_esign/recipients.model");
module.exports.index = async (req, res) => {
  const { accountId } = res.locals.token;

  //   draft , completed and, pendidng
  const draft = await recipientModel
    .query()
    .where("staff_id", accountId)
    .andWhere("status", "draft")
    .count("siasn_esign.recipients.id");

  const completed = await recipientModel
    .query()
    .where("staff_id", accountId)
    .andWhere("status", "completed")
    .count("siasn_esign.recipients.id");

  const pending = await recipientModel
    .query()
    .where("staff_id", accountId)
    .andWhere("status", "on progress")
    .andWhere("signatory_status", "in progress")
    .count("siasn_esign.recipients.id");

  const totalDraft =
    draft.length === 0 ? 0 : draft[0]["count(`siasn_esign`.`recipients`.`id`)"];

  const totalCompleted =
    completed.length === 0
      ? 0
      : completed[0]["count(`siasn_esign`.`recipients`.`id`)"];

  const totalPending =
    pending.length === 0
      ? 0
      : pending[0]["count(`siasn_esign`.`recipients`.`id`)"];

  return res.status(200).json({
    message: "success",
    data: {
      draft: totalDraft,
      completed: totalCompleted,
      pending: totalPending,
    },
  });
};
