const { validationResult } = require("express-validator");
const { nanoid } = require("nanoid");
const { PDFDocument } = require("pdf-lib");
const barcodeSign = require("../../../helpers/esign/barcode-sign");
const times = require("lodash/times");
const biodata = require("../../../models/siasn_master/biodata.model");
const createRecipients = require("../../../helpers/esign/create-recipients");
const { ipInfo } = require("../../../helpers/esign/activities");

// esign models
const documentModel = require("../../../models/siasn_esign/documents.model");
const recipientsModel = require("../../../models/siasn_esign/recipients.model");
const historiesModel = require("../../../models/siasn_esign/document_histories.model");

module.exports.post = async (req, res, next) => {
  const client = req.app.locals.minioClient;
  const errors = validationResult(req);
  const trx = await documentModel.startTransaction();

  if (errors.isEmpty()) {
    try {
      /**
       * create footer documents
       */

      const filename = `${nanoid()}.pdf`;
      const buffer = req.file.buffer;

      const user = await biodata
        .query(trx)
        .select("nip_baru", "nama")
        .where("pegawai_id", res.locals.token.accountId)
        .first();

      const imageScale = 0.35;
      const padding = 5;

      const pdfDoc = await PDFDocument.load(buffer);
      const images = await barcodeSign(req.body.workflow, filename, user);

      const jpgImageBytes = images.toBuffer();
      const jpgImage = await pdfDoc.embedPng(jpgImageBytes);

      // get original image height and width
      const originalHeight = jpgImage.scale(imageScale).height;
      const originalWidth = jpgImage.scale(imageScale).width;

      const totalPage = pdfDoc.getPageCount();
      const pages = pdfDoc.getPages();

      times(totalPage, (n) => {
        const serverPage = pages[n];
        serverPage.drawImage(jpgImage, {
          y: padding,
          x: serverPage.getWidth() - (originalWidth + padding),
          width: originalWidth,
          height: originalHeight,
        });
      });

      pdfDoc.setAuthor(user.nama);
      pdfDoc.setCreationDate(new Date());
      pdfDoc.setCreator("Esign Apps");
      pdfDoc.setProducer("http://esign.bkd.jatimprov.go.id");
      pdfDoc.setTitle(req.file.originalname);

      const result = await pdfDoc.save();
      /**
       * end of create footer documents
       */

      const documentData = {
        title: req.body.title
          ? req.body.title
          : req.file.originalname.split(".").slice(0, -1).join("."),
        status: "draft",
        workflow: req.body.workflow,
        size: req.file.size,
        uploader: res.locals.token.accountId,
        initial_document: filename,
        ongoing_document: filename,
        document_pages: totalPage,
        type: req.body.workflow === "selfSign" ? "parallel" : null,
      };

      const currentDocument = await documentModel
        .query(trx)
        .insertAndFetch(documentData);

      // just single workflow
      const recipients = createRecipients({
        workflow: currentDocument.workflow,
        pegawaiId: res.locals.token.accountId,
        documentId: currentDocument.id,
        upload_date: currentDocument.upload_date,
        document_title: currentDocument.title,
      });

      if (recipients) {
        await recipientsModel.query(trx).insert(recipients);
      }

      const uploadPromise = (...args) => {
        return new Promise((resolve, reject) => {
          client.putObject(...args, function (error) {
            if (error) {
              reject(error);
            } else {
              resolve({
                uid: currentDocument.id,
                name: currentDocument.title,
                status: "done",
                response: { status: "success" },
              });
            }
          });
        });
      };

      const userIpInformation = ipInfo(req);
      const dataHistories = {
        ...userIpInformation,
        staff_id: res.locals.token.accountId,
        document_id: currentDocument.id,
        type: "document",
        action: "uploaded",
      };

      await historiesModel.query(trx).insert(dataHistories);

      const docUpload = await uploadPromise(
        "esign",
        filename,
        Buffer.from(result)
      );

      await trx.commit();
      return res
        .status(200)
        .json({ code: 200, message: "success", data: docUpload });
    } catch (e) {
      await trx.rollback();
      return res
        .status(400)
        .json({ code: 400, message: "Internal Server Error" });
    }
  } else {
    return res.status(442).json({ errors: errors.array() });
  }
};
