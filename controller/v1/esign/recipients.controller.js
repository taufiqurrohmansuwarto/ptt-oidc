const documentModel = require("../../../models/siasn_esign/documents.model");
const recipientModel = require("../../../models/siasn_esign/recipients.model");

const {
  serializeRecipients,
} = require("../../../helpers/esign/serialize_document");

module.exports.index = async (req, res) => {
  try {
    const { documentId } = req.params;
    const document = await documentModel
      .query()
      .select("type", "id")
      .findById(documentId)
      .withGraphFetched("[recipients.[user.[fileDiri]]]")
      .modifyGraph("recipients", (builder) => {
        builder
          .select(
            "id",
            "staff_id",
            "role",
            "signatory_status",
            "is_approve",
            "status",
            "is_owner"
          )
          .whereNot("role", null)
          .orderBy("sequence", "asc");
      })
      .modifyGraph("[recipients.[user]]", (builder) => {
        builder.select("nama", "nip_baru");
      })
      .modifyGraph("[recipients.[user.[fileDiri]]]", (builder) => {
        builder.select("file_foto");
      });

    if (!document) {
      return res.status(404).json({ code: 404, message: "document not found" });
    }

    return res.status(200).json({
      code: 200,
      message: "success",
      data: serializeRecipients(document),
    });
  } catch (e) {
    return res
      .status(400)
      .json({ code: 400, message: "Internal Server Error" });
  }
};

const {
  recipientNotificationEmail,
  recipentNotificationWhatsapp,
} = require("../../../helpers/esign/send_notification");

// update the recipietns when the workflow is not self sign
module.exports.create = async (req, res) => {
  const { documentId } = req.params;
  const { accountId } = res.locals.token;
  // const messageClient = req.app.locals.whatsAppClient;

  // todo better check this recipients
  const { type, recipients } = req.body;

  // this route not workflow self sign

  // recipients harus mempunyai role
  const trx = await documentModel.startTransaction();
  try {
    const currentDocument = await documentModel
      .query()
      .findById(documentId)
      .andWhere("uploader", accountId)
      .andWhere("workflow", "!=", "selfSign");

    // check status dari dokumen
    const { status } = await recipientModel
      .query()
      .where("document_id", documentId)
      .andWhere("is_owner", true)
      .andWhere("staff_id", accountId)
      .first();

    if (!currentDocument || status !== "draft") {
      return res.status(404).json({ code: 404, message: "document not found" });
    }

    // update type dari dokumen itu apa dia menggunakan serial atau pararel dan set signature dari recipients masing2 individu
    await documentModel
      .query(trx)
      .patch({
        type,
      })
      .where("id", documentId);

    // because mysql doesnt support bactch insert the solution is manual
    let recipientsWithDocumentId;

    if (type === "serial") {
      recipientsWithDocumentId = recipients.map((r, index) => ({
        ...r,
        document_id: documentId,
        sequence: index + 1,
        signatory_status: "in progress",
        status: "on progress",
        uploader: currentDocument.uploader,
        upload_date: currentDocument.upload_date,
        document_title: currentDocument.title,
      }));
    } else {
      recipientsWithDocumentId = recipients.map((r) => ({
        ...r,
        document_id: documentId,
        signatory_status: "in progress",
        status: "on progress",
        uploader: currentDocument.uploader,
        upload_date: currentDocument.upload_date,
        document_title: currentDocument.title,
      }));
    }

    await recipientModel.query(trx).insertGraph(recipientsWithDocumentId);
    await recipientModel
      .query(trx)
      .patch({ status: "on progress" })
      .where("document_id", documentId)
      .andWhere("is_owner", true);

    await trx.commit();
    // await recipientNotificationEmail({ accountId, recipients, documentId });

    // await recipentNotificationWhatsapp({
    //   whatsAppClient: messageClient,
    //   accountId,
    //   recipients,
    //   documentId,
    // });

    return res.status(200).json({
      code: 200,
      message: "successfully add recipient",
      data: {
        type: "Add recipients",
        date: new Date(),
      },
    });
  } catch (e) {
    console.log(e);
    await trx.rollback();
    return res
      .status(400)
      .json({ code: "400", message: "Internal Server Error" });
  }
};
