const documentModel = require("../../../models/siasn_esign/documents.model");
const biodataModel = require("../../../models/siasn_master/biodata.model");
const otpModel = require("../../../models/siasn_esign/otp.model");

const { sendWhatsappOTP } = require("../../../helpers/transport");

// histories
const historiesModel = require("../../../models/siasn_esign/document_histories.model");
const { ipInfo } = require("../../../helpers/esign/activities");

const { totp, authenticator } = require("otplib");
totp.options = { step: 180 };

const trim = require("lodash/trim");
const nodemailer = require("nodemailer");

module.exports.sendOtp = async (req, res) => {
  try {
    const { accountId } = res.locals.token;
    const { documentId } = req.params;

    const client = req.app.locals.whatsAppClient;

    const document = await documentModel
      .query()
      .findById(documentId)
      .withGraphFetched("recipients")
      .modifyGraph("recipients", (builder) => {
        builder.select("staff_id", "is_owner", "status");
      });

    const { nama, email, no_hp } = await biodataModel
      .query()
      .where("pegawai_id", accountId)
      .first();

    if (!email || !no_hp) {
      return res
        .status(422)
        .json({ code: 422, message: "Can't find email or phone number" });
    }

    if (!document) {
      return res.status(404).json({ code: 404, message: "document not found" });
    }

    if (Array.isArray(document.recipients)) {
      const recipientOwner = document.recipients.find((d) => !!d.is_owner);
      const userInList = document.recipients.some(
        (d) => parseInt(d.staff_id) === parseInt(accountId)
      );

      // if the current user is in the list and there is the recipient owner
      const validation =
        recipientOwner && recipientOwner.status !== "completed" && userInList;

      if (!validation) {
        return res.status(403).json({
          code: "403",
          message:
            "this document is not belongs to you or document status is already completed",
        });
      }
    }

    const secret = authenticator.generateSecret();
    await otpModel.query().insert({
      staff_id: accountId,
      secret,
      document_id: document.id,
    });

    const transporter = nodemailer.createTransport({
      service: "gmail",
      auth: {
        user: process.env.EMAIL_USERNAME,
        pass: process.env.EMAIL_PASSWORD,
      },
    });

    const token = totp.generate(secret);

    // const mailOptions = {
    //   from: process.env.EMAIL_USERNAME,
    //   to: trim(email).toLowerCase(),
    //   subject: "Kode OTP Esign",
    //   html: `<h1>Hi ${nama} ini adalah token rahasia untuk melakukan persetujuan pada aplikasi esign. Token kamu adalah ${token}</h1>`,
    // };

    // await transporter.sendMail(mailOptions);

    await sendWhatsappOTP(client, { nama, email, no_hp }, token, "OTP");

    const ipInformation = ipInfo(req);
    const data = {
      ...ipInformation,
      document_id: documentId,
      type: "document",
      action: "request otp",
      staff_id: res.locals.token.accountId,
    };

    await historiesModel.query().insert(data);

    return res.status(200).json({
      code: 200,
      message: "Sucessfully sending otp to email",
      data: { documentId: document.id },
    });
  } catch (e) {
    console.log(e);
    return res
      .status(400)
      .json({ code: 400, message: "Internal Server Error" });
  }
};
