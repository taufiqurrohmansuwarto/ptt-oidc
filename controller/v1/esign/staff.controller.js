const biodataModel = require("../../../models/siasn_master/biodata.model");

module.exports.index = async (req, res) => {
  const { nip } = req.params;
  try {
    const staff = await biodataModel
      .query()
      .select("nip_baru", "nama", "biodata.pegawai_id")
      .where("nip_baru", nip)
      .andWhere("blokir", "N")
      .andWhere("aktif", "Y")
      .withGraphJoined("fileDiri")
      .modifyGraph("fileDiri", (builder) => {
        builder.select("file_foto");
      })
      .first();

    let data;
    if (staff) {
      data = {
        id: staff.pegawai_id,
        nip: staff.nip_baru,
        name: staff.nama,
        profile_picture: `https://master.bkd.jatimprov.go.id/files_jatimprov/${staff.fileDiri.file_foto}`,
      };
    } else {
      data = null;
    }

    return res.status(200).json({ code: 200, message: "success", data });
  } catch (e) {
    console.log(e);
    return res
      .status(400)
      .json({ code: 400, message: "Internal Server Error" });
  }
};
