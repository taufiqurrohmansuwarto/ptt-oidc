const notificationModel = require("../../../models/siasn_esign/notifications.model");

module.exports.index = async (req, res) => {
  try {
    const { accountId } = res.locals.token;
    const page = req.query.page || 0;
    const pageSize = req.query.pageSize || 10;
    const is_read = req.query.is_read || false;

    const result = await notificationModel
      .query()
      .where("staff_id", accountId)
      .andWhere("is_read", is_read)
      .orderBy("created_at", "desc")
      .limit(pageSize)
      .offset(page * pageSize);

    return res.status(200).json({
      code: 200,
      message: "success",
      data: result,
    });
  } catch (e) {
    return res
      .status(400)
      .json({ code: 400, message: "Internal Server Error" });
  }
};
