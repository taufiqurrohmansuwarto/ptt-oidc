const { check } = require("../../../helpers/esign/esign_api");
module.exports.index = async (req, res) => {
  try {
    const { nik } = req.params;
    const result = await check(nik);
    const { data } = result;
    return res.status(200).json(data);
  } catch (e) {
    return res
      .status(400)
      .json({ code: 400, message: "Internal Server Error" });
  }
};
