const recipientModel = require("../../../models/siasn_esign/recipients.model");
const documentModel = require("../../../models/siasn_esign/documents.model");
const biodataModel = require("../../../models/siasn_master/biodata.model");

const { ipInfo } = require("../../../helpers/esign/activities");

const {
  descriptionApprovalStatus,
} = require("../../../helpers/esign/approval-status");

const {
  approveReviewer,
  rejectReviewer,
  documentApproveSign,
} = require("../../../helpers/esign/document-serial");

// status approval
module.exports.index = async (req, res) => {
  const { documentId } = req.params;
  const { accountId } = res.locals.token;

  try {
    const result = await recipientModel
      .query()
      .where("document_id", documentId)
      .andWhere("staff_id", accountId)
      .withGraphFetched("document")
      .modifyGraph("document", (builder) => {
        builder.select("status", "type");
      })
      .first();

    const { type, workflow } = await documentModel.query().findById(documentId);
    const listRecipients = await recipientModel
      .query()
      .where("document_id", documentId)
      .withGraphFetched("[user, rejected_user]")
      .modifyGraph("[user]", (builder) => {
        builder.select("nip_baru", "nama");
      })
      .modifyGraph("[rejected_user]", (builder) => {
        builder.select("nip_baru", "nama");
      });

    if (!result) {
      return res.status(404).json({ code: 404, message: "document not found" });
    }

    const description = descriptionApprovalStatus(
      listRecipients,
      workflow,
      type,
      accountId
    );

    return res.status(200).json({
      code: 200,
      message: "success",
      data: { ...result, ...description },
    });
  } catch (e) {
    return res
      .status(400)
      .json({ code: 400, message: "Internal Server Error" });
  }
};

module.exports.approveReview = async (req, res) => {
  const { documentId } = req.params;
  const { accountId } = res.locals.token;

  // properti
  const currentDocument = await documentModel.query().findById(documentId);
  const listRecipient = await recipientModel
    .query()
    .where("document_id", documentId);
  const currentUser = await recipientModel
    .query()
    .where("document_id", documentId)
    .andWhere("staff_id", accountId)
    .andWhereNot("role", null)
    .first();

  if (!currentDocument || !currentUser) {
    return res.status(404).json({ code: 404, message: "document not found" });
  }

  const document = {
    documentId: currentDocument.id,
    workflow: currentDocument.workflow,
    type: currentDocument.type,
  };

  const ipInformation = { ...ipInfo(req) };
  const historyInformation = {
    staff_id: accountId,
    ...ipInformation,
    document_id: currentDocument.id,
    type: "document",
    action: "reviewed",
  };

  if (document.type === "serial") {
    return approveReviewer(
      res,
      null,
      "i agree to this document",
      document,
      listRecipient,
      currentUser,
      historyInformation
    );
  } else {
    return res.status(400).json({ message: "method not implemented" });
  }
};

module.exports.rejectReview = async (req, res) => {
  const { documentId } = req.params;
  const { reason } = req.body;
  const { accountId } = res.locals.token;
  const client = req.app.locals.minioClient;

  // properties
  const currentDocument = await documentModel.query().findById(documentId);

  const listRecipient = await recipientModel
    .query()
    .where("document_id", documentId)
    .withGraphFetched("[user]")
    .modifyGraph("[user]", (builder) => {
      builder.select("nama", "email", "no_hp");
    });

  const currentUser = await recipientModel
    .query()
    .where("document_id", documentId)
    .andWhere("staff_id", accountId)
    .andWhereNot("role", null)
    .first();

  const { nama } = await biodataModel.query().findById(parseInt(accountId));

  if (!currentDocument || !currentUser) {
    return res.status(404).json({ code: 404, message: "document not found" });
  }

  const documentData = {
    client,
    currentUserName: nama,
    documentName: currentDocument.ongoing_document,
  };

  const document = {
    documentId: currentDocument.id,
    workflow: currentDocument.workflow,
    type: currentDocument.type,
  };

  const ipInformation = { ...ipInfo(req) };
  const historyInformation = {
    staff_id: accountId,
    ...ipInformation,
    document_id: currentDocument.id,
    type: "document",
    action: "rejected",
  };

  if (document.type === "serial") {
    return rejectReviewer(
      res,
      null,
      reason ? reason : "i disagree to this document",
      document,
      listRecipient,
      currentUser,
      historyInformation,
      documentData
    );
  } else {
    return res
      .status(400)
      .json({ code: 400, message: "method not implemented" });
  }
};

module.exports.approveSign = async (req, res) => {
  const { documentId } = req.params;
  const { reason, otp_code } = req.body;
  const { accountId } = res.locals.token;
  const client = req.app.locals.minioClient;

  // properties
  const currentDocument = await documentModel.query().findById(documentId);

  const listRecipient = await recipientModel
    .query()
    .where("document_id", documentId)
    .withGraphFetched("[user]")
    .modifyGraph("[user]", (builder) => {
      builder.select("nama", "email", "no_hp");
    });

  const currentUser = await recipientModel
    .query()
    .where("document_id", documentId)
    .andWhere("staff_id", accountId)
    .andWhereNot("role", null)
    .first();

  const { nama } = await biodataModel.query().findById(parseInt(accountId));

  if (!currentDocument || !currentUser) {
    return res.status(404).json({ code: 404, message: "document not found" });
  }

  if (currentUser.role !== "signer") {
    return res
      .status(403)
      .json({
        code: 403,
        message: "cannot sign the document because your role is not signer",
      });
  }

  const documentData = {
    client,
    accountId,
    documentName: currentDocument.ongoing_document,
    signCoordinate: JSON.parse(currentUser.sign_coordinate),
  };

  const document = {
    documentId: currentDocument.id,
    workflow: currentDocument.workflow,
    type: currentDocument.type,
  };

  const ipInformation = { ...ipInfo(req) };
  const historyInformation = {
    staff_id: accountId,
    ...ipInformation,
    document_id: currentDocument.id,
    type: "document",
    action: "rejected",
  };

  if (document.type === "serial") {
    return documentApproveSign(
      res,
      null,
      reason ? reason : "i disagree to this document",
      document,
      listRecipient,
      currentUser,
      historyInformation,
      documentData,
      otp_code
    );
  } else {
    return res
      .status(400)
      .json({ code: 400, message: "method not implemented" });
  }
};
