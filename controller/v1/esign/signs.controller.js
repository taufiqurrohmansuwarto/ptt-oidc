/**
 * untuk kepentingan pribadi dan
 */

const { validationResult } = require("express-validator");
const { PDFDocument } = require("pdf-lib");
const { draw } = require("../../../helpers/esign/sign-symbol");

// api untuk bsre
const fs = require("fs");
const { sign } = require("../../../helpers/esign/esign_api");

const documentModel = require("../../../models/siasn_esign/documents.model");
const otpModel = require("../../../models/siasn_esign/otp.model");
const recipientsModel = require("../../../models/siasn_esign/recipients.model");

// histories
const { ipInfo } = require("../../../helpers/esign/activities");
const historiesModel = require("../../../models/siasn_esign/document_histories.model");
const { totp } = require("otplib");

module.exports.approveSign = async (req, res) => {
  const errors = validationResult(req);
  const { accountId } = res.locals.token;
  const client = req.app.locals.minioClient;

  if (errors.isEmpty()) {
    try {
      const { reason, properties, otp_code } = req.body;
      const documentId = req.params.documentId;

      // callback to promise
      const download = (...args) => {
        return new Promise((resolve, reject) => {
          client.getObject(...args, function (err, stream) {
            if (err) {
              reject(err);
            } else {
              let buffers = [];
              stream.on("data", function (chunk) {
                buffers.push(chunk);
              });
              stream.on("end", function () {
                const buffer = Buffer.concat(buffers);
                resolve(buffer);
              });
              stream.on("error", function (err) {
                reject(err);
              });
            }
          });
        });
      };

      // callback to promise
      const upload = (...args) => {
        return new Promise((resolve, reject) => {
          client.putObject(...args, function (err) {
            if (err) {
              reject(err);
            } else {
              resolve(true);
            }
          });
        });
      };

      const document = await documentModel
        .query()
        .findById(documentId)
        .withGraphFetched("recipients")
        .withGraphFetched("recipients", (builder) => {
          builder.select(
            "id",
            "staff_id",
            "role",
            "signatory_status",
            "status",
            "is_owner"
          );
        });

      /**
       * dokumen checker
       */
      if (!document) {
        return res
          .status(404)
          .json({ code: 404, message: "document not found" });
      }

      if (Array.isArray(document.recipients)) {
        // cek document status is not completed and thereis at least one signatory with type signer
      }

      const { secret } = await otpModel
        .query()
        .where("document_id", document.id)
        .andWhere("staff_id", accountId)
        .orderBy("created_at", "desc")
        .first();

      const isValid = totp.check(otp_code.toString(), secret);

      if (!secret || !isValid) {
        return res.status(400).json({ code: 400, message: "otp is not valid" });
      }

      const initialDocumentBuffer = await download(
        "esign",
        document.initial_document
      );

      /**
       * ambil dari data minio dengan menggunakan array buffer
       */
      const pdfDoc = await PDFDocument.load(initialDocumentBuffer);

      // IMPLEMENTASI BSRE ADA DISINI

      const images = await draw(accountId);
      const jpgImageBytes = images.toBuffer();
      const jpgImage = await pdfDoc.embedPng(jpgImageBytes);

      const originalImageHeight = jpgImage.scale(1).height;
      const originalImageWidth = jpgImage.scale(1).width;
      const pages = pdfDoc.getPages();
      const data = properties;

      data.map(({ xPos: x, yPos: y, height, width, page }) => {
        const imageProperty = {
          height: height / originalImageHeight,
          width: width / originalImageWidth,
        };

        const clientCoord = { x, y };
        const serverPage = pages[page - 1];
        const serverCoord = {
          x,
          y: serverPage.getHeight() - (clientCoord.y + height).toFixed(2),
        };

        const serverData = {
          x: serverCoord.x,
          y: serverCoord.y,
          width: jpgImage.scale(imageProperty.width).width,
          height: jpgImage.scale(imageProperty.height).height,
        };

        serverPage.drawImage(jpgImage, serverData);
      });

      const pdfBytes = await pdfDoc.save();
      const documentSignName = `sign_${document.initial_document}`;

      // INI BAGIAN UNTUK IMPLEMENTASI BSRE
      const currentFiles = Buffer.from(pdfBytes);
      const result = await sign(currentFiles);
      const {
        data: { base64_file },
      } = result;

      await upload(
        "esign",
        documentSignName,
        Buffer.from(base64_file, "base64")
      );
      const ipInformation = ipInfo(req);
      const { ip_address, useragent } = ipInformation;

      // update siapa yang melakukan tanda tangan
      await recipientsModel
        .query()
        .patch({
          signatory_status: "completed",
          reason,
          total_sign_pages: properties.length,
          sign_pages: JSON.stringify(properties.map((props) => props.page)),
          approval_date: new Date(),
          is_approve: true,
          ip_address,
          device: useragent,
          sign_coordinate: JSON.stringify(properties),
        })
        .where("document_id", document.id)
        .andWhere("staff_id", accountId)
        .andWhere("role", "signer");

      // check document if the workflow is signSelf then complete the task in the owner
      if (document.workflow === "selfSign") {
        await recipientsModel
          .query()
          .patch({
            status: "completed",
          })
          .where("staff_id", accountId)
          .andWhere("document_id", document.id)
          .andWhere("is_owner", true);
      }

      // and fucking change in the document main table
      await documentModel
        .query()
        .patch({ sign_document: documentSignName })
        .where("id", document.id);

      // todo insert into the main history
      const historyInformation = {
        ...ipInformation,
        staff_id: res.locals.token.accountId,
        document_id: documentId,
        type: "document",
        action: "signed",
      };

      await historiesModel.query().insert(historyInformation);

      return res.status(200).json({
        code: 200,
        message: "success sign this document",
        // idk what i should do
        data: { ...req.body, documentId: document.id, status: "completed" },
      });
    } catch (e) {
      console.log(e);
      return res
        .status(400)
        .json({ code: 200, message: "Internal Server Error" });
    }
  } else {
    return res.status(422).json({ errors: errors.array() });
  }
};

module.exports.rejectSign = async (req, res) => {};
