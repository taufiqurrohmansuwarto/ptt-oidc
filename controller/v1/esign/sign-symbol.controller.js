const { draw } = require("../../../helpers/esign/sign-symbol");

module.exports.index = async (req, res) => {
  try {
    const { accountId } = res.locals.token;
    const currentDraw = await draw(accountId);
    const data = Buffer.from(currentDraw.toBuffer()).toString("base64");
    return res.status(200).json({
      message: "success",
      data,
      code: 200,
    });
  } catch (e) {
    console.log(e);
    return res.status(400).json({ message: "internal server errors" });
  }
};

module.exports.get = async (req, res) => {
  try {
    const { nip } = req.params;
    const currentDraw = await draw(nip);
    const data = Buffer.from(currentDraw.toBuffer()).toString("base64");
    return res.status(200).json({
      message: "success",
      data,
      code: 200,
    });
  } catch (e) {
    console.log(e);
    return res.status(400).json({ message: "internal server errors" });
  }
};
