const riwayatPendidikan = require("../../models/siasn_master/rwyt_pendidikan.model");

module.exports.index = async (req, res) => {
  const {
    token: { iat, exp, accountId, scope },
  } = res.locals;

  const data = await riwayatPendidikan
    .query()
    .where("pegawai_id", accountId)
    .withGraphFetched("jenjang")
    .modifyGraph("jenjang", (builder) => {
      builder.select("jenjang_id", "jenjang_pendidikan", "kode_bkn");
    });

  res.json({
    message: "succes",
    data,
    code: 200,
  });
};

module.exports.details = async (req, res) => {
  const { id } = req.params;
  const {
    token: { accountId },
  } = res.locals;

  const result = await riwayatPendidikan
    .query()
    .where("pegawai_id", accountId)
    .andWhere("pendidikan_id", id)
    .withGraphFetched("jenjang")
    .modifyGraph("jenjang", (builder) => {
      builder.select("jenjang_id", "jenjang_pendidikan", "kode_bkn");
    })
    .first();

  if (!result) {
    res.status(400).json({
      code: 400,
      message: "Data not found",
    });
  }

  res.status(200).json({
    code: 200,
    message: "success",
    data: result,
  });
};

module.exports.patch = async (req, res) => {};
// active
module.exports.put = async (req, res) => {};
module.exports.delete = async (req, res) => {};
