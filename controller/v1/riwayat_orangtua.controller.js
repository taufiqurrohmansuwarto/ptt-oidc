const orangTuaModel = require("../../models/siasn_master/rwyt_orangtua.model");

module.exports.index = async (_, res) => {
  const { token } = res.locals;
  const { accountId } = token;

  const result = await orangTuaModel.query().where("pegawai_id", accountId);

  if (!result) {
    res.status(400).send({ message: "Data Not Found", code: 400 });
  }

  res.status(200).json({
    message: "success",
    data: result,
    code: 200,
  });
};

module.exports.patch = async (req, res) => {};
module.exports.put = async (req, res) => {};
module.exports.delete = async (req, res) => {};
module.exports.details = async (req, res) => {};
