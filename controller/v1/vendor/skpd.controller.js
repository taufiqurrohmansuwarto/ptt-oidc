const skpdModel = require("../../../models/ptt/skpd.model");
const arrayToTree = require("array-to-tree");
const { has } = require("lodash");
const cache = require("../../../lib/node-cache");

module.exports.index = async (req, res) => {
  let data;
  if (!cache.get("skpd")) {
    const skpd = await skpdModel
      .query()
      .select("id", "pId", "name")
      .orderBy("id", "asc");
    data = skpd;
    cache.set("skpd", skpd);
  } else {
    data = cache.get("skpd");
  }

  if (has(req.query, "tree")) {
    data = arrayToTree(data, {
      parentProperty: "pId",
      customID: "id",
    });
  }

  return res.json({
    code: 200,
    message: "success",
    data,
  });
};
