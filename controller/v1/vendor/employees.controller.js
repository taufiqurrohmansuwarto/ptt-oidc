const Biodata = require("../../../models/siasn_master/biodata.model");

module.exports.full = async (req, res) => {
  const result = await Biodata.query()
    .where("aktif", "Y")
    .andWhere("blokir", "N");

  res.status(200).json({ code: 200, data: [] });
};
