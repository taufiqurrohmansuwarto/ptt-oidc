const skpdModel = require("../../models/siasn_ref/skpd.model");

module.exports.index = async (req, res) => {
  const {
    token: { iat, exp, accountId, scope },
  } = res.locals;

  const skpd = await skpdModel.query();
  res.json({
    message: "succes",
    data: skpd,
    code: 200,
  });
};
