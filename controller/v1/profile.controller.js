const biodataModel = require("../../models/siasn_master/biodata.model");

module.exports.index = async (_, res) => {
  const {
    token: { accountId },
  } = res.locals;

  const user = await biodataModel
    .query()
    .where("pegawai_id", accountId)
    .first()
    .select("nama", "nip_baru");
  res.json({
    message: "success",
    code: 200,
    data: user,
  });
};
