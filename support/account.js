const biodataModel = require("../models/ptt/ptt-biodata.model");
const toString = require("lodash/toString");

const crypto = require("crypto");
const pttAccountDetail = require("../helpers/ptt-account-detail");
const md5 = (data) => {
  return crypto.createHash("md5").update(data).digest("hex");
};

class Account {
  static async findByLogin(login, password) {
    const user = await biodataModel
      .query()
      .where("niptt", login)
      .select("id_ptt", "niptt", "password")
      .where("aktif", "Y")
      .andWhere("blokir", "N")
      .first();

    if (!user) {
      return undefined;
    } else {
      if (md5(password) === user.password) {
        return {
          accountId: toString(user.id_ptt),
        };
      } else {
        return undefined;
      }
    }
  }

  static async findAccount(ctx, id, token) {
    try {
      const user = await biodataModel
        .query()
        .where("id_ptt", id)
        .first()
        .where("aktif", "Y")
        .andWhere("blokir", "N")
        .first();

      if (!user) {
        return undefined;
      }

      // eslint-disable-line no-unused-vars
      // token is a reference to the token used for which a given account is being loaded,
      //   it is undefined in scenarios where account claims are returned from authorization endpoint
      // ctx is the koa request context
      return {
        accountId: id,
        async claims(use, scope) {
          const result = await pttAccountDetail(user.id_ptt);
          return {
            sub: id,
            ...result,
            // email: user.email || "pegawai@jatimprov.go.id",
            // email_verified: !!user.email,
            // phone_number: user.no_hp || "00000000",
            // phone_number_verified: !!user.no_hp,
            // nip: user.nip_baru,
          };
        },
      };
    } catch (e) {
      console.log(e);
    }
  }
}

module.exports = Account;
