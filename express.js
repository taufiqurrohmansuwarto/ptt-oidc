/* eslint-disable no-console */

const path = require("path");
const url = require("url");
const rewrite = require("express-urlrewrite");
require("dotenv").config();

const set = require("lodash/set");
const express = require("express"); // eslint-disable-line import/no-unresolved
require("express-async-errors");
const helmet = require("helmet");
const cors = require("cors");

const { Provider } = require("oidc-provider");

const Account = require("./support/account");
const configuration = require("./support/configuration");
const routes = require("./routes/express");

const origin = "http://localhost:8080";
const mountPath = "";
const issuer = origin + mountPath;

const { PORT = 3000, ISSUER = `http://iput.com` } = process.env;
configuration.findAccount = Account.findAccount;

// adding some ip information
const expressip = require("express-ip");
const useragent = require("express-useragent");

const app = express();

app.use(express.static(__dirname + "/public"));
app.use(helmet());
app.use(cors());
app.use(expressip().getIpInfoMiddleware);
app.use(useragent.express());

app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");

let server;

(async () => {
  const adapter = require("./adapters/sequelize");

  // const authSession = fs.existsSync(
  //   path.join(__dirname, "./tokens/session.data.json")
  // );

  adapter.connect();

  app.locals.baseUrl = process.env.baseUrl;

  const provider = new Provider(issuer, { adapter, ...configuration });

  if (process.env.NODE_ENV === "production") {
    app.enable("trust proxy");
    provider.proxy = true;
    set(configuration, "cookies.short.secure", true);
    set(configuration, "cookies.long.secure", true);

    app.use((req, res, next) => {
      if (req.secure) {
        next();
      } else if (req.method === "GET" || req.method === "HEAD") {
        res.redirect(
          url.format({
            protocol: "https",
            host: req.get("host"),
            pathname: req.originalUrl,
          })
        );
      } else {
        res.status(400).json({
          error: "invalid_request",
          error_description: "do yourself a favor and only use https",
        });
      }
    });
  }

  routes(app, provider);
  // rewrite
  // app.use(rewrite("/.well-known/*", "/oauth2/.well-known/$1"));
  app.locals.baseUrl = process.env.baseUrl;
  app.use("/oidc", provider.callback);

  // const client = await venom.create("iput");
  // app.locals.whatsAppClient = client;

  server = app.listen(PORT, () => {
    console.log(
      `application is listening on port ${PORT}, check its /.well-known/openid-configuration`
    );
  });
})().catch((err) => {
  if (server && server.listening) server.close();
  console.error(err);
  process.exitCode = 1;
});
