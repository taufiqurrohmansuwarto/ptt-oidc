const express = require("express");
const router = express.Router();

// import controller yang sesuai
const skpdController = require("../../controller/v1/skpd.controller");

router.route("/").get(skpdController.index);

module.exports = router;
