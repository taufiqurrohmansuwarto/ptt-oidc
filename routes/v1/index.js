const express = require("express");
const router = express.Router();
const profile = require("./profile.route");
const skpd = require("./skpd.route");
const riwayatPendidikan = require("./riwayat_pendidikan.route");
const riwayatOrangtua = require("./riwayat_orangtua.route");

const esign = require("./esign");

// integrasi data kabkot
const kabkot = require("./kabkot");

// siasn esign
router.use("/esign", esign);
router.use("/profile", profile);
router.use("/skpd", skpd);
router.use("/pendidikan", riwayatPendidikan);
router.use("/orangtua", riwayatOrangtua);

// integrasi kabkot
router.use("/kabkot", kabkot);

// integrasi vendor
const vendor = require("./vendor/index");
router.use("/vendor", vendor);

module.exports = router;
