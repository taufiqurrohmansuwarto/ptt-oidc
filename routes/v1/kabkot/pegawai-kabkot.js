const express = require("express");
const router = express.Router();

const pegawaiKabkotController = require("../../../controller/v1/kabkot/pegawai_kabkot.controller");

router.route("/").get(pegawaiKabkotController.index);

module.exports = router;
