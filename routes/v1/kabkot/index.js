const express = require("express");
const router = express.Router();

const pegawaiKabkot = require("./pegawai-kabkot");

const {
  validateScope,
} = require("../../../middlewares/authentication.middleware");

router.use(validateScope(["kabkot"]), (req, res, next) => {
  const { token } = res.locals;
  if (token.isClientCredentials) {
    next();
  } else {
    res.append("WWW-Authenticate", "");
    return res.status(403).send("grant types is not client credentials");
  }
});

router.use("/pegawai-kabkot", pegawaiKabkot);

module.exports = router;
