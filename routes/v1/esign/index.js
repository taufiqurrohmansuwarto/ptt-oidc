const express = require("express");
const router = express.Router();

const documentsRoute = require("./documents.route");
const uploadRoute = require("./uploads.route");
const signSymbolRoute = require("./sign-symbol.route");
const activitiesRoute = require("./histories.route");
const dashboardRoute = require("./dashboard.route");
const staffRoute = require("./staff.route");
const notificationRoute = require("./notifications.route");
const tteRoute = require("./tte.route");

// scope validation
const {
  validateScope,
} = require("../../../middlewares/authentication.middleware");

// hanya scope esign yang dapat mengakses halaman ini
router.use(validateScope(["esign"]));

// next you must create some middleware to check if scope esign is existed
router.use("/uploads", uploadRoute);
router.use("/documents", documentsRoute);
router.use("/sign-symbol", signSymbolRoute);
router.use("/activities", activitiesRoute);
router.use("/dashboard", dashboardRoute);
router.use("/staff", staffRoute);
router.use("/notifications", notificationRoute);
router.use("/tte", tteRoute);

module.exports = router;
