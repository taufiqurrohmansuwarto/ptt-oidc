const { param, body } = require("express-validator");

module.exports.approveSignValidation = [
  param("documentId").notEmpty().withMessage(":documentId must be required"),
  body("reason")
    .notEmpty()
    .withMessage("reason cannot be empty")
    .isString()
    .withMessage("reason must be string"),
  body("properties")
    .isArray()
    .withMessage("properties must be an array of object")
    .notEmpty()
    .withMessage("properties is required must be at least one position object"),
  body("properties.*.xPos")
    .isFloat()
    .withMessage("xPos must be float")
    .notEmpty()
    .withMessage("xPos cannot be empty"),
  body("properties.*.yPos")
    .isFloat()
    .withMessage("yPos must be float")
    .notEmpty()
    .withMessage("yPos cannot be empty"),
  body("properties.*.page").isInt().notEmpty(),
  body("properties.*.width").isFloat().notEmpty(),
  body("properties.*.height").isFloat().notEmpty(),
  body("otp_code").notEmpty().withMessage("otp code is required"),
];

module.exports.rejectSignValidation = [];
