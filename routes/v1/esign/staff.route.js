const express = require("express");
const router = express.Router();
const staffController = require("../../../controller/v1/esign/staff.controller");

router.route("/:nip").get(staffController.index);

module.exports = router;
