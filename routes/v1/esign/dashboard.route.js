const express = require("express");
const router = express.Router();
const dashboardController = require("../../../controller/v1/esign/dashboard.controller");
router.route("/").get(dashboardController.index);

module.exports = router;
