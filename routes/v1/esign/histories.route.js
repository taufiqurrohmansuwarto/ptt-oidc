const express = require("express");
const router = express.Router();

const activityController = require("../../../controller/v1/esign/histories.controller");

router.route("/").get(activityController.activities);

module.exports = router;
