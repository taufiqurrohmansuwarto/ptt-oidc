const express = require("express");
const router = express.Router();

const documentController = require("../../../controller/v1/esign/documents.controller");
const discussionsController = require("../../../controller/v1/esign/discussions.controller");
const recipientController = require("../../../controller/v1/esign/recipients.controller");
const signsController = require("../../../controller/v1/esign/signs.controller");
const otpController = require("../../../controller/v1/esign/otp.controller");
const historyController = require("../../../controller/v1/esign/histories.controller");
const approvalController = require("../../../controller/v1/esign/approval.controller");

const bodyParser = require("body-parser");
const { approveSignValidation } = require("./documents.validation");

router.use(bodyParser.json());
router.route("/").get(documentController.index);

router
  .route("/:documentId")
  .get(documentController.getDetails)
  .patch(documentController.archieved)
  .delete(documentController.delete);

router.route("/:documentId/download").get(documentController.download);

router
  .route("/:documentId/discussions")
  .get(discussionsController.index)
  .post(discussionsController.create);

router.route("/:documentId/histories").get(historyController.historyDocument);
router.route("/:documentId/status-approval").get(approvalController.index);

// recipients
router
  .route("/:documentId/recipients")
  .get(recipientController.index)
  .post(recipientController.create);
router.route("/:documentId/otp").get(otpController.sendOtp);

router
  .route("/:documentId/sign-request")
  .put([...approveSignValidation], signsController.approveSign)
  .delete(signsController.rejectSign);

// reviewer
router
  .route("/:documentId/reviewer-request")
  .put(approvalController.approveReview)
  .delete(approvalController.rejectReview);

// signer
router.route("/:documentId/signer-request").put(approvalController.approveSign);
// .delete(approvalController.rejectSign);

module.exports = router;
