const { checkSchema, check } = require("express-validator");

module.exports.post = [
  [
    check("workflow")
      .notEmpty()
      .withMessage("workflow cannot be empty!")
      .isIn(["selfSign", "singAndRequest", "requestFromOthers"])
      .withMessage(
        "workflow value must be selfSign, signAnRequest, requestFromOtherss"
      ),
    check("title").optional().isString(),
  ],
  checkSchema({
    file: {
      custom: {
        options: (_, { req }) => !!req.file,
        errorMessage: "You should upload one file with .pdf format",
      },
    },
  }),
];
