const express = require("express");
const router = express.Router();
const notificationController = require("../../../controller/v1/esign/notification.controller");

router.route("/").get(notificationController.index);

module.exports = router;
