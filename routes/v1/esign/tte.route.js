const express = require("express");
const router = express.Router();
const tteController = require("../../../controller/v1/esign/tte.controller");

router.route("/:nik").get(tteController.index);

module.exports = router;
