const express = require("express");
const router = express.Router();
const signSymbolController = require("../../../controller/v1/esign/sign-symbol.controller");

router.route("/").get(signSymbolController.index);

// get tanda tangan
router.route("/:nip").get(signSymbolController.get);

module.exports = router;
