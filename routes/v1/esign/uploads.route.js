const express = require("express");
const router = express.Router();
const uploadController = require("../../../controller/v1/esign/uploads.controller");

const validation = require("./uploads.validation");
const Multer = require("multer");
const path = require("path");

// checking upload file
const uploadFile = (req, res, next) => {
  const upload = Multer({
    storage: Multer.memoryStorage(),
    fileFilter: (req, file, cb) => {
      const ext = path.extname(file.originalname);
      if (ext !== ".pdf") {
        cb(null, false, new Error("File type must be .pdf format"));
      } else {
        cb(null, true);
      }
    },
  }).single("file");

  upload(req, res, function (err) {
    if (err instanceof Multer.MulterError) {
      return res.status(422).send({ err });
    } else if (err) {
      return res.status(422).json({ err: err });
    } else {
      next();
    }
  });
};

router.route("/").post(uploadFile, [...validation.post], uploadController.post);

module.exports = router;
