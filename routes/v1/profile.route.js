const express = require("express");
const router = express.Router();

// import controller yang sesuai
const profileController = require("../../controller/v1/profile.controller");

router.route("/").get(profileController.index);

module.exports = router;
