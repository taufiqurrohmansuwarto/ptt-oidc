const express = require("express");
const { full } = require("../../../controller/v1/vendor/employees.controller");
const router = express.Router();

const {
  validateScope,
} = require("../../../middlewares/authentication.middleware");

router.route("/cache").get(validateScope(["admin.employees.full:read"]), full);

module.exports = router;
