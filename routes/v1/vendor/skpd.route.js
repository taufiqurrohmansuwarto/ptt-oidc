const express = require("express");
const router = express.Router();
const skpdController = require("../../../controller/v1/vendor/skpd.controller");

const {
  validateScope,
} = require("../../../middlewares/authentication.middleware");

router
  .route("/")
  .get(validateScope(["admin.departments.full:read"]), skpdController.index);

module.exports = router;
