const express = require("express");
const router = express.Router();

const skpdRoute = require("./skpd.route");
const employeeRoute = require("./employees.route");

const {
  isClientCredentials,
} = require("../../../middlewares/authentication.middleware");

router.use(isClientCredentials);

router.use("/skpd", skpdRoute);
router.use("/employees", employeeRoute);

module.exports = router;
