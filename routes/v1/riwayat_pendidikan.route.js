const express = require("express");
const router = express.Router();

// import controller yang sesuai
const riwyatPendidikanController = require("../../controller/v1/riwayat_pendidikan.controller");

router.route("/").get(riwyatPendidikanController.index);

router
  .route("/:id")
  .get(riwyatPendidikanController.details)
  // this method using to activate something
  .put(riwyatPendidikanController.put)
  .patch(riwyatPendidikanController.patch)
  .delete(riwyatPendidikanController.delete);

module.exports = router;
