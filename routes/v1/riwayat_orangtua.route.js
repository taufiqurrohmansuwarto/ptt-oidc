const express = require("express");
const router = express.Router();
const orangtuaController = require("../../controller/v1/riwayat_orangtua.controller");

router.route("/").get(orangtuaController.index);
router.route("/:id").get().patch().put().delete();

module.exports = router;
