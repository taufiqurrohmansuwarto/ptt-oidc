/* eslint-disable no-console, max-len, camelcase, no-unused-vars */
const { strict: assert } = require("assert");
const querystring = require("querystring");
const { inspect } = require("util");
const apiVersion1 = require("./v1/index");

const isEmpty = require("lodash/isEmpty");
const { urlencoded } = require("express"); // eslint-disable-line import/no-unresolved
require("dotenv").config();

const Account = require("../support/account");

const body = urlencoded({ extended: false });

const keys = new Set();
const debug = (obj) =>
  querystring.stringify(
    Object.entries(obj).reduce((acc, [key, value]) => {
      keys.add(key);
      if (isEmpty(value)) return acc;
      acc[key] = inspect(value, { depth: null });
      return acc;
    }, {}),
    "<br/>",
    ": ",
    {
      encodeURIComponent(value) {
        return keys.has(value) ? `<strong>${value}</strong>` : value;
      },
    }
  );

module.exports = (app, provider) => {
  const {
    constructor: {
      errors: { SessionNotFound },
    },
  } = provider;

  app.use((req, res, next) => {
    const orig = res.render;
    // you'll probably want to use a full blown render engine capable of layouts
    res.render = (view, locals) => {
      app.render(view, locals, (err, html) => {
        if (err) throw err;
        orig.call(res, "_layout", {
          ...locals,
          body: html,
        });
      });
    };
    next();
  });

  function setNoCache(req, res, next) {
    res.set("Pragma", "no-cache");
    res.set("Cache-Control", "no-cache, no-store");
    next();
  }

  app.get(`/interaction/:uid`, setNoCache, async (req, res, next) => {
    try {
      const {
        uid,
        prompt,
        params,
        session,
      } = await provider.interactionDetails(req, res);
      const crypto = require("crypto");

      const client = await provider.Client.find(params.client_id);
      const iput = crypto.randomBytes(16).toString("base64");

      switch (prompt.name) {
        case "select_account": {
          if (!session) {
            return provider.interactionFinished(
              req,
              res,
              { select_account: {} },
              { mergeWithLastSubmission: false }
            );
          }

          const account = await provider.Account.findAccount(
            undefined,
            session.accountId
          );
          const { email } = await account.claims(
            "prompt",
            "email",
            { email: null },
            []
          );

          return res.render("select_account", {
            client,
            uid,
            iput,
            email,
            details: prompt.details,
            params,
            title: "Sign-in",
            session: session ? debug(session) : undefined,
            dbg: {
              params: debug(params),
              prompt: debug(prompt),
            },
          });
        }
        case "login": {
          return res.render("login", {
            iput,
            client,
            uid,
            details: prompt.details,
            params,
            title: "",
            session: session ? debug(session) : undefined,
            dbg: {
              params: debug(params),
              prompt: debug(prompt),
            },
            flash: undefined,
          });
        }
        case "consent": {
          return res.render("interaction", {
            iput,
            client,
            uid,
            details: prompt.details,
            params,
            title: "Authorize",
            session: session ? debug(session) : undefined,
            dbg: {
              params: debug(params),
              prompt: debug(prompt),
            },
          });
        }
        default:
          return undefined;
      }
    } catch (err) {
      return next(err);
    }
  });

  app.post(
    `/interaction/:uid/login`,
    setNoCache,
    body,
    async (req, res, next) => {
      try {
        const {
          uid,
          params,
          prompt,
          session,
        } = await provider.interactionDetails(req, res);
        const { name } = prompt;

        assert.equal(name, "login");
        const account = await Account.findByLogin(
          req.body.login,
          req.body.password
        );
        const client = await provider.Client.find(params.client_id);

        if (!account) {
          res.render("login", {
            client,
            uid,
            session,
            details: prompt.details,
            params: {
              ...params,
              login_hint: req.body.email,
            },
            dbg: {
              params: debug(params),
              prompt: debug(prompt),
            },
            title: "Sign-in",
            flash:
              "Username atau password anda salah. Atau akun anda sedang diblokir",
          });
          return;
        } else {
          const result = {
            select_account: {}, // make sure its skipped by the interaction policy since we just logged in
            login: {
              account: account.accountId,
            },
          };

          await provider.interactionFinished(req, res, result, {
            mergeWithLastSubmission: false,
          });
        }
      } catch (err) {
        next(err);
      }
    }
  );

  app.post(
    `/interaction/:uid/continue`,
    setNoCache,
    body,
    async (req, res, next) => {
      try {
        const interaction = await provider.interactionDetails(req, res);
        const {
          prompt: { name, details },
        } = interaction;
        assert.equal(name, "select_account");

        if (req.body.switch) {
          if (interaction.params.prompt) {
            const prompts = new Set(interaction.params.prompt.split(" "));
            prompts.add("login");
            interaction.params.prompt = [...prompts].join(" ");
          } else {
            interaction.params.prompt = "login";
          }
          await interaction.save();
        }

        const result = { select_account: {} };
        await provider.interactionFinished(req, res, result, {
          mergeWithLastSubmission: false,
        });
      } catch (err) {
        next(err);
      }
    }
  );

  app.post(
    `/interaction/:uid/confirm`,
    setNoCache,
    body,
    async (req, res, next) => {
      try {
        const {
          prompt: { name, details },
        } = await provider.interactionDetails(req, res);
        assert.equal(name, "consent");

        const consent = {};

        // any scopes you do not wish to grant go in here
        //   otherwise details.scopes.new.concat(details.scopes.accepted) will be granted
        consent.rejectedScopes = [];

        // any claims you do not wish to grant go in here
        //   otherwise all claims mapped to granted scopes
        //   and details.claims.new.concat(details.claims.accepted) will be granted
        consent.rejectedClaims = [];

        // replace = false means previously rejected scopes and claims remain rejected
        // changing this to true will remove those rejections in favour of just what you rejected above
        consent.replace = false;

        const result = { consent };
        await provider.interactionFinished(req, res, result, {
          mergeWithLastSubmission: true,
        });
      } catch (err) {
        next(err);
      }
    }
  );

  app.get(`/interaction/:uid/abort`, setNoCache, async (req, res, next) => {
    try {
      const result = {
        error: "access_denied",
        error_description: "End-User aborted interaction",
      };
      await provider.interactionFinished(req, res, result, {
        mergeWithLastSubmission: false,
      });
    } catch (err) {
      next(err);
    }
  });

  // middleware
  const {
    getAccessToken,
    getTokenFromRequestHeader,
    validateAccessToken,
  } = require("../middlewares/authentication.middleware");

  const Minio = require("minio");
  require("dotenv").config();
  const client = new Minio.Client({
    endPoint: "localhost",
    port: 9000,
    useSSL: false,
    accessKey: process.env.MINIO_ACCESSKEY,
    secretKey: process.env.MINIO_SECRETKEY,
  });

  app.locals.minioClient = client;

  app.use(
    "/v1/api",
    [getTokenFromRequestHeader, getAccessToken(provider), validateAccessToken],
    apiVersion1
  );

  const objectionError = require("../lib/error-handler.js");

  app.use((err, req, res, next) => {
    if (err instanceof SessionNotFound) {
      // handle interaction expired / session not found error
      // res.set("WWW-Authenticate", 'Basic realm="401"'); // change this
      // res.status(401).send();
    }

    objectionError(err, res);

    next(err);
  });
};
