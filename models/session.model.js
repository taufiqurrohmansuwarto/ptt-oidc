const knex = require("../config/knex");
const { Model } = require("objection");
Model.knex(knex);

class Session extends Model {
  static get tableName() {
    return "Session";
  }
}

module.exports = Session;
