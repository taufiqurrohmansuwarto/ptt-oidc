const { Model } = require("objection");
const knex = require("../../config/knex-siasn");

Model.knex(knex);

class RiwayatOrangTua extends Model {
  static get tableName() {
    return "siasn_master.rwyt_orangtua";
  }

  static get idColumn() {
    return "siasn_master.rwyt_orangtua.orangtua_id";
  }

  static get relationMappings() {
    const pekerjaan = require("../siasn_ref/pekerjaan.model");

    return {
      pekerjaan_ayah: {
        modelClass: pekerjaan,
        relation: Model.BelongsToOneRelation,
        join: {
          from: "siasn_maseter.rwyt_orangtua.pekerjaan_id_ayah",
          to: "siasn_ref.ref_pekerjaan.pekerjaan_id",
        },
      },
      pekerjaan_ibu: {
        modelClass: pekerjaan,
        relation: Model.BelongsToOneRelation,
        join: {
          from: "",
          to: "",
        },
      },
    };
  }
}

module.exports = RiwayatOrangTua;
