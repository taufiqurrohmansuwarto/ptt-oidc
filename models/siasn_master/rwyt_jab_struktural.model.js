const { Model } = require("objection");
const knex = require("../../config/knex-siasn");

Model.knex(knex);

class RiwayatJabatanStruktural extends Model {
  static get tableName() {
    return "siasn_master.rwyt_jab_struktural";
  }

  static get idColumn() {
    return "struktural_id";
  }

  static get relationMappings() {
    const jabatanStruktural = require("../siasn_ref/jabatan_struktural.model");
    return {
      jabatan_struktural: {
        relation: Model.BelongsToOneRelation,
        modelClass: jabatanStruktural,
        join: {
          from: "siasn_master.rwyt_jab_struktural.jab_struktural_id",
          to: "siasn_ref.ref_jabatan_struktural.jab_struktural_id",
        },
      },
    };
  }
}

module.exports = RiwayatJabatanStruktural;
