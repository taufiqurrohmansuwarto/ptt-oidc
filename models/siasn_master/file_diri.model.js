const { Model } = require("objection");
const knex = require("../../config/knex-siasn");

Model.knex(knex);

class FileDiri extends Model {
  static get tableName() {
    return "siasn_master.file_diri";
  }

  static get idColumn() {
    return "siasn_master.file_diri.file_id";
  }
}

module.exports = FileDiri;
