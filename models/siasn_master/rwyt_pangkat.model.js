const { Model } = require("objection");
const knex = require("../../config/knex-siasn");

Model.knex(knex);

class RiwayatPangkat extends Model {
  static get tableName() {
    return "siasn_master.rwyt_pangkat";
  }

  static get relationMappings() {
    const pangkatModel = require("../siasn_ref/pangkat.model");
    return {
      pangkat: {
        relation: Model.BelongsToOneRelation,
        modelClass: pangkatModel,
        join: {
          from: "siasn_master.rwyt_pangkat.pangkat_id",
          to: "siasn_ref.ref_pangkat.pangkat_id",
        },
      },
    };
  }

  static get idColumn() {
    return "kp_id";
  }
}

module.exports = RiwayatPangkat;
