const knex = require("../config/knex");
const { Model } = require("objection");
Model.knex(knex);

class ReplayDetection extends Model {
  static get tableName() {
    return "ReplayDetection";
  }
}

module.exports = ReplayDetection;
