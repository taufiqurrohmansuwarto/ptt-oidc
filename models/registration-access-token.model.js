const knex = require("../config/knex");
const { Model } = require("objection");
Model.knex(knex);

class RegistrationAccessToken extends Model {
  static get tableName() {
    return "RegistrationAccessToken";
  }
}

module.exports = RegistrationAccessToken;
