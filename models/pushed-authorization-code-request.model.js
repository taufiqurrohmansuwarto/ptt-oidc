const knex = require("../config/knex");
const { Model } = require("objection");
Model.knex(knex);

class PushedAuthorizationCodeRequest extends Model {
  static get tableName() {
    return "PushedAuthorizationCodeRequest";
  }
}

module.exports = PushedAuthorizationCodeRequest;
