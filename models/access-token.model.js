const knex = require("../config/knex");
const { Model } = require("objection");
Model.knex(knex);

class AccessToken extends Model {
  static get tableName() {
    return "AccessToken";
  }
}

module.exports = AccessToken;
