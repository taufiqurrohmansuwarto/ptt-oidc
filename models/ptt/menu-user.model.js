const knex = require("../../config/knex-siasn");
const { Model } = require("objection");

Model.knex(knex);

class MenuUser extends Model {
  static get tableName() {
    return "menu_user";
  }

  static get idColumn() {
    return "id";
  }
}

module.exports = MenuUser;
