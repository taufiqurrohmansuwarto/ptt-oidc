const knex = require("../../config/knex-siasn");
const { Model } = require("objection");

Model.knex(knex);

class MenuAdmin extends Model {
  static get tableName() {
    return "menu_admin";
  }

  static get idColumn() {
    return "id";
  }
}

module.exports = MenuAdmin;
