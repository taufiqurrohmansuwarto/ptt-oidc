const knex = require("../../config/knex-siasn");
const { Model } = require("objection");

Model.knex(knex);

class RwytSuamiIstri extends Model {
  static get tableName() {
    return "rwyt_suami_istri";
  }

  static get relationMappings() {
    const RefPekerjaan = require("./ref-pekerjaan.model");
    return {
      pekerjaan: {
        modelClass: RefPekerjaan,
        relation: Model.BelongsToOneRelation,
        join: {
          from: "rwyt_suami_istri.pekerjaan_id",
          to: "ref_pekerjaan.pekerjaan_id",
        },
      },
    };
  }

  static get idColumn() {
    return "suami_istri_id";
  }
}

module.exports = RwytSuamiIstri;
