const knex = require("../../config/knex-siasn");
const { Model } = require("objection");

Model.knex(knex);

class Jenjang extends Model {
  static get tableName() {
    return "jenjang";
  }

  static get idColumn() {
    return "id_jenjang";
  }
}

module.exports = Jenjang;
