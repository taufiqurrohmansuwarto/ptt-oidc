const knex = require("../../config/knex-siasn");
const { Model } = require("objection");

Model.knex(knex);

class LogPtt extends Model {
  static get tableName() {
    return "log_ptt";
  }

  static get idColumn() {
    return "log_id";
  }
}

module.exports = LogPtt;
