const { Model } = require("objection");
const knex = require("../../config/knex-siasn");

Model.knex(knex);

class JabatanFungsionalTerapan extends Model {
  static get tableName() {
    return "siasn_ref.ref_jft";
  }

  static get modifiers() {
    return {
      profile(builder) {
        builder.select("id", "name");
      },
    };
  }

  static get idColumn() {
    return "id";
  }
}

module.exports = JabatanFungsionalTerapan;
