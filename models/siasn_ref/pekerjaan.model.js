const { Model } = require("objection");
const knex = require("../../config/knex-siasn");

Model.knex(knex);

class Pekerjaan extends Model {
  static get tableName() {
    return "siasn_ref.ref_pekerjaan";
  }

  static get idColumn() {
    return "siasn_ref.ref_pekerjaan.pekerjaan_id";
  }
}

module.exports = Pekerjaan;
