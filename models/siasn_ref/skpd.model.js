const { Model } = require("objection");
const knex = require("../../config/knex-siasn");

Model.knex(knex);

class Skpd extends Model {
  static get tableName() {
    return "siasn_ref.skpd";
  }

  static get idColumn() {
    return "siasn_ref.skpd.idk";
  }

  static get modifiers() {}
}

module.exports = Skpd;
