const { Model } = require("objection");
const knex = require("../../config/knex-siasn");

Model.knex(knex);

class KabupatenKota extends Model {
  static get tableName() {
    return "siasn_ref.kabkot";
  }

  static get idColumn() {
    return "siasn_ref.kabkot.id_kabkot";
  }
}

module.exports = KabupatenKota;
