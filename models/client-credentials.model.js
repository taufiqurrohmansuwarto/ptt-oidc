const knex = require("../config/knex");
const { Model } = require("objection");
Model.knex(knex);

class ClientCredentials extends Model {
  static get tableName() {
    return "ClientCredentials";
  }
}

module.exports = ClientCredentials;
