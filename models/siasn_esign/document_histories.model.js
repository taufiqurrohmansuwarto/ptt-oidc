const { nanoid } = require("nanoid");
const { Model } = require("objection");
const knex = require("../../config/knex-siasn");

Model.knex(knex);

class DocumentHistories extends Model {
  static get tableName() {
    return "siasn_esign.histories";
  }

  $beforeInsert() {
    this.id = nanoid();
  }

  static get relationMappings() {
    const biodataModel = require("../siasn_master/biodata.model");
    const documentModel = require("../siasn_esign/documents.model");

    return {
      document: {
        modelClass: documentModel,
        relation: Model.BelongsToOneRelation,
        join: {
          from: "siasn_esign.histories.document_id",
          to: "siasn_esign.documents.id",
        },
      },
      user: {
        modelClass: biodataModel,
        relation: Model.BelongsToOneRelation,
        join: {
          from: "siasn_esign.histories.staff_id",
          to: "siasn_master.biodata.pegawai_id",
        },
      },
    };
  }
}

module.exports = DocumentHistories;
