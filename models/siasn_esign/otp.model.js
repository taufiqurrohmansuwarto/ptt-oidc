const { Model } = require("objection");
const knex = require("../../config/knex-siasn");

Model.knex(knex);

class Otp extends Model {
  static get tableName() {
    return "siasn_esign.otp";
  }
}

module.exports = Otp;
