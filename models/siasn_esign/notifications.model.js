const { nanoid } = require("nanoid");
const { Model } = require("objection");
const knex = require("../../config/knex-siasn");

Model.knex(knex);

class notifications extends Model {
  static get tableName() {
    return "siasn_esign.notifications";
  }

  static get idColumn() {
    return "id";
  }
}

module.exports = notifications;
