const { Model } = require("objection");
const knex = require("../../config/knex-siasn");

Model.knex(knex);

class StaffSigns extends Model {
  static get tableName() {
    return "siasn_esign.staff_signs";
  }

  static get idColumn() {
    return "staff_id";
  }
}

module.exports = StaffSigns;
