const { nanoid } = require("nanoid");
const { Model } = require("objection");
const knex = require("../../config/knex-siasn");

Model.knex(knex);

class Documents extends Model {
  static get tableName() {
    return "siasn_esign.documents";
  }

  static get modifiers() {
    return {
      doc(builder) {
        builder.select("id", "title", "upload_date", "type", "workflow");
      },
    };
  }

  static get relationMappings() {
    const recipients = require("./recipients.model");
    const discussions = require("./discussions.model");
    const uploader = require("../siasn_master/biodata.model");

    return {
      owner: {
        modelClass: uploader,
        relation: Model.BelongsToOneRelation,
        join: {
          from: "siasn_esign.documents.uploader",
          to: "siasn_master.biodata.pegawai_id",
        },
      },
      discussions: {
        modelClass: discussions,
        relation: Model.HasManyRelation,
        join: {
          from: "siasn_esign.documents.id",
          to: "siasn_esign.discussions.document_id",
        },
      },
      recipients: {
        modelClass: recipients,
        relation: Model.HasManyRelation,
        join: {
          from: "siasn_esign.documents.id",
          to: "siasn_esign.recipients.document_id",
        },
      },
    };
  }

  $beforeInsert() {
    this.id = nanoid();
  }
}

module.exports = Documents;
