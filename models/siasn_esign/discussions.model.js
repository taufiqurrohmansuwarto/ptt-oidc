const { nanoid } = require("nanoid");
const { Model } = require("objection");
const knex = require("../../config/knex-siasn");

Model.knex(knex);

class Discussions extends Model {
  static get tableName() {
    return "siasn_esign.discussions";
  }

  $beforeInsert() {
    this.id = nanoid();
  }

  static get relationMappings() {
    const biodata = require("../siasn_master/biodata.model");
    return {
      user: {
        modelClass: biodata,
        relation: Model.BelongsToOneRelation,
        join: {
          from: "siasn_esign.discussions.staff_id",
          to: "siasn_master.biodata.pegawai_id",
        },
      },
      children: {},
    };
  }
}

module.exports = Discussions;
