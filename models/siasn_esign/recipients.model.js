const { nanoid } = require("nanoid");
const { Model } = require("objection");
const knex = require("../../config/knex-siasn");

Model.knex(knex);

class Recipients extends Model {
  static get tableName() {
    return "siasn_esign.recipients";
  }

  static get relationMappings() {
    const biodataModel = require("../siasn_master/biodata.model");
    const documentModel = require("./documents.model");

    return {
      document: {
        modelClass: documentModel,
        relation: Model.BelongsToOneRelation,
        join: {
          from: "siasn_esign.recipients.document_id",
          to: "siasn_esign.documents.id",
        },
      },
      user: {
        modelClass: biodataModel,
        relation: Model.BelongsToOneRelation,
        join: {
          from: "siasn_esign.recipients.staff_id",
          to: "siasn_master.biodata.pegawai_id",
        },
      },
      rejected_user: {
        modelClass: biodataModel,
        relation: Model.BelongsToOneRelation,
        join: {
          from: "siasn_esign.recipients.rejected_by",
          to: "siasn_master.biodata.pegawai_id",
        },
      },
    };
  }

  $beforeInsert() {
    this.id = nanoid();
  }
}

module.exports = Recipients;
