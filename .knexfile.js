require("dotenv").config();

const user = process.env.ptt_username;
const password = process.env.ptt_password;
const database = process.env.ptt_name;
const port = process.env.ptt_port;
const host = process.env.ptt_host;

const connection = {
  database,
  user,
  password,
  port,
  host,
};

module.exports = {
  siasn: {
    client: "mysql",
    connection,
    pool: {
      min: 2,
      max: 10,
    },
    migrations: {
      tableName: "knex_migrations",
    },
  },
  development: {
    client: "mysql",
    connection,
    pool: {
      min: 2,
      max: 10,
    },
    migrations: {
      tableName: "knex_migrations",
    },
  },
  staging: {
    client: "mysql",
    connection,
    pool: {
      min: 2,
      max: 10,
    },
    migrations: {
      tableName: "knex_migrations",
    },
  },
  production: {
    client: "mysql",
    connection,
    pool: {
      min: 2,
      max: 10,
    },
  },
};
