const AccessToken = require("../models/access-token.model");
const AuthorizationCode = require("../models/authorization-code.model");
const ClientCredentials = require("../models/client-credentials.model");
const Client = require("../models/client.model");
const DeviceCode = require("../models/device-code.model");
const InitialAccessToken = require("../models/initial-access-token.model");
const Interaction = require("../models/interaction.model");
const PushedAuthorizationCodeRequest = require("../models/pushed-authorization-code-request.model");
const RefreshToken = require("../models/refresh-token.model");
const RegistrationAccessToken = require("../models/registration-access-token.model");
const ReplayDetection = require("../models/replay-detection.model");
const Session = require("../models/session.model");

const db = require("../config/knex");
const upsert = require("knex-upsert");

const models = new Map();
models.set("AccessToken", AccessToken);
models.set("AuthorizationCode", AuthorizationCode);
models.set("ClientCredentials", ClientCredentials);
models.set("Client", Client);
models.set("DeviceCode", DeviceCode);
models.set("InitialAccessToken", InitialAccessToken);
models.set("Interaction", Interaction);
models.set("PushedAuthorizationCodeRequest", PushedAuthorizationCodeRequest);
models.set("RefreshToken", RefreshToken);
models.set("RegistrationAccessToken", RegistrationAccessToken);
models.set("ReplayDetection", ReplayDetection);
models.set("Session", Session);

class Objection {
  constructor(name) {
    this.model = models.get(name);
    this.name = name;
  }

  async upsert(id, data, expiresIn) {
    const havingUid = await db.schema.hasColumn(this.name, "uid");
    const havingUserCode = await db.schema.hasColumn(this.name, "userCode");
    const havingGrantId = await db.schema.hasColumn(this.name, "grantId");

    if (havingUid) {
      await upsert({
        db,
        table: this.name,
        key: "id",
        object: {
          id,
          data: JSON.stringify(data),
          ...(data.uid ? { uid: data.uid } : undefined),
          ...(expiresIn ? { expiresAt: new Date(Date.now() + expiresIn * 1000) } : undefined),
        },
      });
    }
    if (havingGrantId) {
      await upsert({
        db,
        table: this.name,
        key: "id",
        object: {
          id,
          data: JSON.stringify(data),
          ...(data.grantId ? { grantId: data.grantId } : undefined),
          ...(expiresIn ? { expiresAt: new Date(Date.now() + expiresIn * 1000) } : undefined),
        },
      });
    }
    if (havingUserCode) {
      await upsert({
        db,
        table: this.name,
        key: "id",
        object: {
          id,
          data: JSON.stringify(data),
          ...(data.userCode ? { userCode: data.userCode } : undefined),
          ...(expiresIn ? { expiresAt: new Date(Date.now() + expiresIn * 1000) } : undefined),
        },
      });
    } else {
      await upsert({
        db,
        table: this.name,
        key: "id",
        object: {
          id,
          data: JSON.stringify(data),
          ...(expiresIn ? { expiresAt: new Date(Date.now() + expiresIn * 1000) } : undefined),
        },
      });
    }
  }

  async find(id) {
    const found = await this.model.query().where("id", id).first();
    if (!found) return undefined;

    return {
      ...found.data,
      ...(found.consumedAt ? { consumed: true } : undefined),
    };
  }

  async findByUserCode(userCode) {
    const found = await this.model.query().where("userCode", userCode).first();
    if (!found) return undefined;
    return {
      ...found.data,
      ...(found.consumedAt ? { consumed: true } : undefined),
    };
  }

  async findByUid(uid) {
    const found = await this.model.query().where("uid", uid).first();
    if (!found) return undefined;
    return {
      ...found.data,
      ...(found.consumedAt ? { consumed: true } : undefined),
    };
  }

  async destroy(id) {
    await this.model.query().delete().where("id", id);
  }

  async consume(id) {
    await this.model.query().update({ consumedAt: new Date() }).where("id", id);
  }

  async revokeByGrantId(grantId) {
    await this.model.query().delete().where("grantId", grantId);
  }
}

module.exports = Objection;
